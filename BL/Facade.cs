﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd;

namespace BL
{
    public class Facade
    {
        UserController UC;
        CouponController CC;
        BusinessController BC;
        OrderController OC;

        public Facade()
        {
            this.UC = new UserController();
            this.CC = new CouponController();
            this.BC = new BusinessController();
            this.OC = new OrderController();
        }

        public char LogIn(string city, string password, string email)
        {
            if (UC.CheckPasswordAndMail(password, email))
            {
                return UC.GetUserType(email);
            }
            else
                return 'f' ;
        }

        public AdminClass GetAdmin(string email)
        {
            return UC.GetAdmin(email);
        }

        public BusinessOwnerClass GetBusinessOwner(string email)
        {
            return UC.GetBusinessOwner(email);
        }

        public CustomerClass GetCustomer(string email)
        {
            return UC.GetCustumer(email);
        }

        public Boolean CheckIfUserExist(string email)
        {
            return UC.CheckIfUserExist(email);
        }

        public Boolean RegisterCustumer(string email, string password, string userName, int phone, List<String> userPrefrence)
        {
            return UC.RegisterCustumer(email, password, userName, phone, userPrefrence);
        }

        public string ForgetPassword(string email)
        {
            if (UC.CheckIfUserExist(email))
                return UC.ForgetPassword(email);
            else return null;
        }

        public string UpdatePassword(string email, string oldPassword, string newPassword)
        {
            if (UC.CheckPasswordAndMail(oldPassword,email)){
                if (newPassword != "")
                    return UC.UpdatePassword(email, newPassword);
                else
                    return "enter valid new password";
            }
            else return "wrong password";
        }

        public List<DisplayedCoupon> DisplayCoupons(string category, string city)
        {
            return CC.DisplayCoupons(category, city);
        }

        public List<DisplayedBusiness> DisplayBusinesses(string category, string city)
        {
            return BC.DisplayBusinesses(category, city);
        }

        public List<DisplayedCoupon> CouponsToApprove()
        {
            return CC.CouponsToApprove();
        }

        public Boolean ApproveCoupon(int couponID)
        {
            return CC.ApproveCoupon(couponID);
        }

        public Boolean DeleteCoupon(int couponID)
        {
            return CC.DeleteCoupon(couponID);
        }

        public DisplayedBusiness GetBusiness(int businessID)
        {
           return BC.GetBusiness(businessID);
        }

        public Boolean DeleteBusiness(int BusinessID)
        {
            return (CC.DeleteAllCouponsOfBusiness(BusinessID) &&
                    UC.DeleteBusinessOwnerOfBusiness(BusinessID) &&
                    BC.DeleteBusiness(BusinessID));            
        }

        public Boolean AddNewBusiness(string address, string description, string ID, string Name, string category, string city)
        {
            return BC.AddNewBusiness(address, description, ID, Name, category, city);
        }

        public bool AddBusinessOwner(int Id, string email, string userName, int phone, string password)
        {
            if (BC.checkIfBusinessExist(Id))
            {
                return (UC.AddBusinessOwner(Id, email, userName, phone, password) &&
                UC.addNewUser(email, userName, phone, 'b') &&
                UC.addUserLogin(email, password));                    
            }           
            else return false;
        }

        public DisplayedCoupon GetCoupon(int couponId)
        {
            return CC.GetCoupon(couponId);
        }



        public Boolean OrderCoupon(int couponId, CustomerClass user)
        {
            int serial  = OC.SerialKey(couponId);
            if (OC.AddOrder(user.getEmail(), couponId, serial))
            {
                OC.AddOrderToUser(user, couponId, serial);
                return true;
            }
            else return false;
        }

        public DisplayedCoupon DisplayCoupons(int CouponID)
        {
            return CC.GetCoupon(CouponID);
        }

        public Boolean ChangeCoupon(int CouponID, string newName, string newCategory, string newDescription, int newDiscountPrice, DateTime newExpiredDate)
        {
            return CC.ChangeCoupon(CouponID, newName, newCategory, newDescription, newDiscountPrice, newExpiredDate);
        }

        public List<DisplayOrder> DisplayOrders(string email)
        {
            return OC.DisplayOrders(email);
        }

        public Boolean CheckIfUserHaveCategory(string category, string email)
        {
            return UC.CheckIfUserHaveCategory(category, email);
        }


        public void UpdatePreference(string email, string preference, bool checkedP)
        {
            if (checkedP && !UC.CheckIfUserHaveCategory(preference, email))
            {
                UC.UpdatePreference(email, preference);
            }
            if (!checkedP && UC.CheckIfUserHaveCategory(preference, email))
            {
                UC.DeletePreference(email, preference);
            }
        }

        public List<DisplayRatedCoupons> ToRateCoupons(string email)
        {
            return CC.ToRateCoupons(email);
        }


        public bool RateCoupon(string email, int couponID, int rating)
        {
           return CC.RateCoupon(email, couponID, rating);
        }

        public List<DisplayOrderedCoupons> DisplayCouponOrders(string email)
        {
            return BC.DisplayCouponOrders(email);
        }

        public bool AddCoupon(int businessId, int originalPrice, int discountPrice, string name, string description, string category, DateTime expiredDate)
        {
            int couponID = CC.generateCouponID();
            if (BC.checkIfBusinessExist(businessId) && name != "" && description != "" && category != "" && expiredDate != null)
            {
                return CC.AddCoupon(couponID, businessId, originalPrice, discountPrice, name, description, category, expiredDate);
            }
            else return false;
        }

        public bool UpdateCoustomerUsedCoupon(int serialKey, string email)
        {
            return OC.UpdateCoustomerUsedCoupon(serialKey, email);
        }

        public List<DisplayRatedCoupon> DisplayRatedCoupons(string email)
        {
            return CC.DisplayRatedCoupons(email);
        }

        public List<int> getRating(int couponID)
        {
            return CC.getRating(couponID);
        }

        public bool AddSocialCoupon(string description, string link)
        {
            if (description != "" && link != "")
                return CC.AddSocialCoupon(description, link);
            else return false;

        }

        public List<DisplaySocialCoupon> DisplaySocialCoupons()
        {
            return CC.DisplaySocialCoupons();
        }
    }
}
