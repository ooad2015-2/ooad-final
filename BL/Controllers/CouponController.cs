﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd;

namespace BL
{
    public class CouponController
    {
        private SDAL.SDAL sdal;

        public CouponController()
        {
            this.sdal = new SDAL.SDAL();
        }

        public List<DisplayedCoupon> DisplayCoupons(string category, string city)
        {
            return sdal.DisplayCoupons(category, city);
        }

        public List<DisplayedCoupon> CouponsToApprove()
        {
            return sdal.CouponsToApprove();
        }

        public Boolean ApproveCoupon(int CouponID)
        {
            return sdal.ApproveCoupon(CouponID);
        }

        public Boolean DeleteCoupon(int couponID)
        {
            return sdal.DeleteCoupon(couponID);
        }

        public Boolean DeleteAllCouponsOfBusiness(int BusinessID)
        {
            return sdal.DeleteAllCouponsOfBusiness(BusinessID);
        }

        public DisplayedCoupon GetCoupon(int couponId)
        {
            return sdal.GetDisplayedCoupon(couponId);
        }

        public Boolean ChangeCoupon(int CouponID, string newName, string newCategory, string newDescription, int newDiscountPrice, DateTime newExpiredDate)
        {
            return sdal.ChangeCoupon(CouponID, newName, newCategory, newDescription, newDiscountPrice, newExpiredDate);
        }

        public List<DisplayRatedCoupons> ToRateCoupons(string email)
        {
            return sdal.ToRateCoupons(email);
        }

        public bool RateCoupon(string email, int couponID, int rating)
        {
            return sdal.RateCoupon(email, couponID, rating);
        }

        public int generateCouponID()
        {
            return sdal.generateCouponID();
        }

        public bool AddCoupon(int couponID, int businessId, int originalPrice, int discountPrice, string name, string description, string category, DateTime expiredDate)
        {
            return sdal.AddCoupon(couponID, businessId, originalPrice, discountPrice, name, description, category, expiredDate);
        }

        public List<DisplayRatedCoupon> DisplayRatedCoupons(string email)
        {
            return sdal.DisplayRatedCoupons(email);
        }

        public List<int> getRating(int couponID)
        {
            return sdal.getRating(couponID);
        }

        public bool AddSocialCoupon(string description, string link)
        {
            return sdal.AddSocialCoupon(description, link);
        }

        public List<DisplaySocialCoupon> DisplaySocialCoupons()
        {
            return sdal.DisplaySocialCoupons();
        }
    }
}
