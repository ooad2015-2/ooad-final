﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd;

namespace BL
{
    public class BusinessController
    {
        private SDAL.SDAL sdal;

        public BusinessController()
        {
            this.sdal = new SDAL.SDAL();
        }

        public List<BackEnd.DisplayedBusiness> DisplayBusinesses(string category, string city)
        {
            return sdal.DisplayBusinesses(category, city);
        }

        public DisplayedBusiness GetBusiness(int businessID)
        {
            return sdal.GetBusiness(businessID);
        }

        public Boolean DeleteBusiness(int businessID)
        {
            return sdal.DeleteBusiness(businessID);
        }

        public Boolean AddNewBusiness(string address, string description, string ID, string Name, string category, string city)
        {
            if (address != "" && description != "" && ID != "" && Name != "" && category != "" && city != "")
                return sdal.AddNewBusiness(address, description, ID, Name, category, city);
            else return false;
        }

        public Boolean checkIfBusinessExist(int Id)
        {
            return sdal.checkIfBusinessExist(Id);
        }

        public List<DisplayOrderedCoupons> DisplayCouponOrders(string email)
        {
            return sdal.DisplayCouponOrders(email);
        }
    }
}
