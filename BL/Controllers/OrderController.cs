﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd;

namespace BL
{
    public class OrderController
    {
        private SDAL.SDAL sdal;

        public OrderController()
        {
            this.sdal = new SDAL.SDAL();
        }


        public int SerialKey(int couponId)
        {
            int nuberOfOrders = sdal.TotalOrdersOfCoupon(couponId);
            return Convert.ToInt32((Convert.ToString(nuberOfOrders + 1) + Convert.ToString(couponId)));
        }

        public bool AddOrder(string email, int couponId, int serialKey)
        {
            return sdal.AddOrder(email, couponId, serialKey);
        }

        public void AddOrderToUser(CustomerClass user, int couponId, int serial)
        {
            sdal.AddOrderToUser(user, couponId, serial);
        }

        public List<DisplayOrder> DisplayOrders(string email)
        {
            return sdal.DisplayOrders(email);
        }

        public bool UpdateCoustomerUsedCoupon(int serialKey, string email)
        {
            return sdal.UpdateCoustomerUsedCoupon(serialKey, email);
        }
    }
}
