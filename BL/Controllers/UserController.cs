﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDAL;
using BackEnd;

namespace BL
{
    class UserController
    {
        private SDAL.SDAL sdal;

        public UserController()
        {
            this.sdal = new SDAL.SDAL();
        }

        public Boolean CheckPasswordAndMail(string password, string email)
        {
            return sdal.CheckPasswordAndMail(password, email);
        }

        public char GetUserType(string email)
        {
            return sdal.GetUserType(email);
        }

        public AdminClass GetAdmin(string email)
        {
            return sdal.GetAdmin(email);
        }

        public BusinessOwnerClass GetBusinessOwner(string email)
        {
            return sdal.GetBusinessOwner(email);
        }

        public CustomerClass GetCustumer(string email)
        {
            return sdal.GetCustumer(email);
        }

        public Boolean CheckIfUserExist(string email)
        {
            return sdal.CheckIfUserExist(email);
        }

        public Boolean RegisterCustumer(string email, string password, string userName, int phone, List<String> userPrefrence)
        {
            return sdal.RegisterCustumer(email, password, userName, phone, userPrefrence);
        }

        public string ForgetPassword(string email)
        {
            return sdal.GetPassword(email);
        }

        public string UpdatePassword(string email, string password)
        {
            return sdal.UpdatePassword(email, password);   
        }

        public Boolean DeleteBusinessOwnerOfBusiness(int BusinessID)
        {
            return sdal.DeleteBusinessOwnerOfBusiness(BusinessID);
        }

        public Boolean AddBusinessOwner(int businessId, string email, string userName, int phone, string password)
        {
            if (email != "" && userName != "" && phone != 0 && password != "")
            {
                return sdal.AddBusinessOwner(businessId, email, userName, phone, password);
            }
            else return false;
        }

        public Boolean addUserLogin(string email, string password)
        {
            return sdal.addUserLogin(email, password);
        }

        public Boolean addNewUser(string email, string userName, int phone, char p)
        {
            return sdal.addNewUser(email, userName, phone, p);
        }

        public Boolean CheckIfUserHaveCategory(string category, string email)
        {
            return sdal.CheckIfUserHaveCategory(category, email);
        }

  
        public bool UpdatePreference(string email, string preference)
        {
            return sdal.AddPreferenceToUser(email, preference);
        }

        public bool DeletePreference(string email, string preference)
        {
            return sdal.DeletePreference(email, preference);
        }
    }
}
