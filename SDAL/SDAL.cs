﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using BackEnd;

namespace SDAL
{
    public class SDAL
    {
        DataClasses1DataContext db;

        public SDAL()
        {
            db = new DataClasses1DataContext();
        }

        public Boolean CheckPasswordAndMail(string password, string email)
        {
            var queryLogin = from userLogin in db.UsersLogIns
                             where userLogin.Email == email && userLogin.Password == password
                             select userLogin;

            if (queryLogin.Count() != 0)
                return true;
            else
                return false;
        }

        public AdminClass GetAdmin(string email)
        {
            var queryUser = from user in db.Users
                            where user.Email == email
                            select user;
            AdminClass adminclass = new AdminClass(queryUser.First().Phone, queryUser.First().UserName, queryUser.First().Email);
            return adminclass;
        }

        public char GetUserType(string email)
        {
            var queryUser = from user in db.Users
                            where user.Email == email
                            select user;

            return queryUser.First().Type;
        }

        public BusinessOwnerClass GetBusinessOwner(string email)
        {
            var queryUser = from user in db.Users
                            where user.Email == email
                            select user;


            List<BusinessClass> listOfBusinesses = new List<BusinessClass>();
            var businesses = from business in db.Businesses
                             join businessOwner in db.BusinessOwners on business.ID equals businessOwner.Business_ID
                             where businessOwner.Owner_Email == email
                             select business;
            foreach (var v in businesses)
            {
                BusinessClass bc = new BusinessClass(v.ID, v.Name, v.Address, v.City, v.Description, v.Category);
                listOfBusinesses.Add(bc);
            }

            return new BusinessOwnerClass(listOfBusinesses, queryUser.First().Phone, queryUser.First().UserName, queryUser.First().Email);
        }

        public CustomerClass GetCustumer(string email)
        {
            var queryUser = from user in db.Users
                            where user.Email == email
                            select user;

            var queryOrders = from order in db.Orders
                              where order.User_Email == queryUser.First().Email
                              select order;
            List<Order> orders = queryOrders.ToList();
            List<OrderClass> userOrders = new List<OrderClass>();

            //create customer class with list of orders
            foreach (Order o in orders)
            {
                var queryCoupon = from coupon in db.Coupons
                                  where coupon.ID == o.Coupon_ID
                                  select coupon;

                CouponClass userCoupon = new CouponClass(queryCoupon.First().ID, queryCoupon.First().Name,
                                                         queryCoupon.First().Description, queryCoupon.First().Original_Price,
                                                         queryCoupon.First().Discount_Price, queryCoupon.First().Expired_Date,
                                                         queryCoupon.First().Status);
                OrderClass ord = new OrderClass(o.Serial, o.Order_Date, o.Status, userCoupon);
                userOrders.Add(ord);
            }


            return new CustomerClass(queryUser.First().Phone, queryUser.First().UserName,
                                                       queryUser.First().Email, userOrders);
        }

        public Boolean CheckIfUserExist(string email)
        {
            var queryEmail = from userLogin in db.UsersLogIns
                             where userLogin.Email == email
                             select userLogin;

            return (queryEmail.Count() != 0);
        }

        public Boolean RegisterCustumer(string email, string password, string userName, int phone, List<String> userPrefrence)
        {
            UsersLogIn newUser = new UsersLogIn
            {
                Email = email,
                Password = password
            };
            db.UsersLogIns.InsertOnSubmit(newUser);

            User Tmp = new User
            {
                Email = email,
                Phone = phone,
                UserName = userName,
                Type = 'c'
            };
            db.Users.InsertOnSubmit(Tmp);

            foreach (string s in userPrefrence)
            {
                UserPrefrence UP = new UserPrefrence
                {
                    Prefrence = s,
                    Email = email
                };
                db.UserPrefrences.InsertOnSubmit(UP);
            }
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                db.SubmitChanges();
            }

            throw new NotImplementedException();
        }

        public string GetPassword(string email)
        {
            var queryLogin = from userLogin in db.UsersLogIns
                             where userLogin.Email == email
                             select userLogin;
            return queryLogin.First().Password;
        }

        public string UpdatePassword(string email, string password)
        {
            var updateNewPassword = from userLogin in db.UsersLogIns
                                    where userLogin.Email == email
                                    select userLogin;
            UsersLogIn logIn = updateNewPassword.First();
            logIn.Password = password;
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                db.SubmitChanges();
            }

            return "Password changed successfully";
        }

        public List<DisplayedCoupon> DisplayCoupons(string category, string city)
        {
            if (city != "" && category != "")
            {
                var searchCoupon = from coupon in db.Coupons
                                   join business in db.Businesses on coupon.Business_ID equals business.ID
                                   where coupon.Category == category && coupon.Status == "active"
                                         && business.City == city && coupon.Expired_Date > DateTime.Today
                                   select new DisplayedCoupon()
                                   {
                                       Coupon_Name = coupon.Name,
                                       Coupon_ID = coupon.ID,
                                       Category = coupon.Category,
                                       Business_Name = business.Name,
                                       Description = coupon.Description,
                                       Original_Price = coupon.Original_Price,
                                       Discount_Price = coupon.Discount_Price,
                                       Expired_Date = coupon.Expired_Date
                                   };
                return searchCoupon.ToList();
            }
            else if (city == "" && category != "")
            {
                var searchCoupon = from coupon in db.Coupons
                                   join business in db.Businesses on coupon.Business_ID equals business.ID
                                   where coupon.Category == category && coupon.Status == "active"
                                         && coupon.Expired_Date > DateTime.Today
                                   select new DisplayedCoupon()
                                   {
                                       Coupon_Name = coupon.Name,
                                       Coupon_ID = coupon.ID,
                                       Category = coupon.Category,
                                       Business_Name = business.Name,
                                       Description = coupon.Description,
                                       Original_Price = coupon.Original_Price,
                                       Discount_Price = coupon.Discount_Price,
                                       Expired_Date = coupon.Expired_Date
                                   };
                return searchCoupon.ToList();
            }
            else if (city != "" && category == "")
            {
                var searchCoupon = from coupon in db.Coupons
                                   join business in db.Businesses on coupon.Business_ID equals business.ID
                                   where business.City == city && coupon.Status == "active"
                                         && coupon.Expired_Date > DateTime.Today
                                   select new DisplayedCoupon()
                                   {
                                       Coupon_Name = coupon.Name,
                                       Coupon_ID = coupon.ID,
                                       Category = coupon.Category,
                                       Business_Name = business.Name,
                                       Description = coupon.Description,
                                       Original_Price = coupon.Original_Price,
                                       Discount_Price = coupon.Discount_Price,
                                       Expired_Date = coupon.Expired_Date
                                   };
                return searchCoupon.ToList();
            }
            else if (city == "" && category == "")
            {
                var searchCoupon = from coupon in db.Coupons
                                   join business in db.Businesses on coupon.Business_ID equals business.ID
                                   where coupon.Status == "active" && coupon.Expired_Date > DateTime.Today
                                   select new DisplayedCoupon()
                                   {
                                       Coupon_Name = coupon.Name,
                                       Coupon_ID = coupon.ID,
                                       Category = coupon.Category,
                                       Business_Name = business.Name,
                                       Description = coupon.Description,
                                       Original_Price = coupon.Original_Price,
                                       Discount_Price = coupon.Discount_Price,
                                       Expired_Date = coupon.Expired_Date
                                   };
                return searchCoupon.ToList();
            }
            return null;
        }

        public List<DisplayedBusiness> DisplayBusinesses(string category, string city)
        {
            if (city != "" && category != "")
            {
                var searchBusiness = from business in db.Businesses
                                     where business.Category == category && business.City == city
                                     select new DisplayedBusiness()
                                   {
                                       Business_ID = business.ID,
                                       Business_Name = business.Name,
                                       Category = business.Category,
                                       Description = business.Description,
                                       Address = business.Address,
                                       City = business.City
                                   };
                return searchBusiness.ToList();
            }
            else if (city == "" && category != "")
            {
                var searchBusiness = from business in db.Businesses
                                     where business.Category == category
                                     select new DisplayedBusiness()
                                     {
                                         Business_ID = business.ID,
                                         Business_Name = business.Name,
                                         Category = business.Category,
                                         Description = business.Description,
                                         Address = business.Address,
                                         City = business.City
                                     };
                return searchBusiness.ToList();
            }
            else if (city != "" && category == "")
            {
                var searchBusiness = from business in db.Businesses
                                     where business.City == city
                                     select new DisplayedBusiness()
                                     {
                                         Business_ID = business.ID,
                                         Business_Name = business.Name,
                                         Category = business.Category,
                                         Description = business.Description,
                                         Address = business.Address,
                                         City = business.City
                                     };
                return searchBusiness.ToList();
            }
            else if (city == "" && category == "")
            {
                var searchBusiness = from business in db.Businesses
                                     select new DisplayedBusiness()
                                     {
                                         Business_ID = business.ID,
                                         Business_Name = business.Name,
                                         Category = business.Category,
                                         Description = business.Description,
                                         Address = business.Address,
                                         City = business.City
                                     };
                return searchBusiness.ToList();
            }
            return null;
        }

        public List<DisplayedCoupon> CouponsToApprove()
        {
            var couponsWaitingForAprovel = from coupon in db.Coupons
                                           join business in db.Businesses on coupon.Business_ID equals business.ID
                                           where coupon.Status == "notactive"
                                           select new DisplayedCoupon()
                                           {
                                               Coupon_Name = coupon.Name,
                                               Coupon_ID = coupon.ID,
                                               Category = coupon.Category,
                                               Business_Name = business.Name,
                                               Description = coupon.Description,
                                               Original_Price = coupon.Original_Price,
                                               Discount_Price = coupon.Discount_Price,
                                               Expired_Date = coupon.Expired_Date
                                           };
            return couponsWaitingForAprovel.ToList();
        }

        public Boolean ApproveCoupon(int CouponID)
        {
            Coupon couponToApprove = (from coupons in db.Coupons
                                      where coupons.ID == CouponID
                                      select coupons).ToList().First();
            couponToApprove.Status = "active";
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                db.SubmitChanges();
            }
            return true;
        }

        public Boolean DeleteCoupon(int couponID)
        {
            Coupon couponToDelete = (from coupons in db.Coupons
                                     where coupons.ID == couponID
                                     select coupons).ToList().First();
            couponToDelete.Status = "deleted";
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                db.SubmitChanges();
            }
            return true;
        }

        public DisplayedBusiness GetBusiness(int businessID)
        {
            var getBusiness = from business in db.Businesses
                              where business.ID == businessID
                                 select new DisplayedBusiness()
                                 {
                                     Business_ID = business.ID,
                                     Business_Name = business.Name,
                                     Category = business.Category,
                                     Description = business.Description,
                                     Address = business.Address,
                                     City = business.City
                                 };
            return getBusiness.ToList().First();
        }

        public bool DeleteBusiness(int businessID)
        {

            var DeleteBusiness = from business in db.Businesses
                                 where business.ID == businessID
                                 select business;
                   
            foreach (var Del in DeleteBusiness)
            {
                db.Businesses.DeleteOnSubmit(Del);                
            }

            try
            {
                db.SubmitChanges();
            }
              catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public Boolean DeleteAllCouponsOfBusiness(int BusinessID)
        {
            var DeleteCoupons = from coupon in db.Coupons
                                where coupon.Business_ID == BusinessID
                                select coupon;
            foreach (var Del in DeleteCoupons)
            {
                db.Coupons.DeleteOnSubmit(Del);
            }
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public Boolean DeleteBusinessOwnerOfBusiness(int BusinessID)
        {
            var DeleteBusinessOwner = from businessOwner in db.BusinessOwners
                                   where businessOwner.Business_ID == BusinessID
                                   select businessOwner;

            foreach (var Del in DeleteBusinessOwner)
            {
                db.BusinessOwners.DeleteOnSubmit(Del);
            }
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public Boolean AddNewBusiness(string address, string description, string ID, string Name, string category, string city)
        {
            Business business = new Business
            {
                ID = Convert.ToInt32(ID),
                Address = address,
                City = city,
                Name = Name,
                Description = description,
                Category = category
            };

            db.Businesses.InsertOnSubmit(business);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public Boolean AddBusinessOwner(int Id, string email, string userName, int phone, string password)
        {
            BusinessOwner businessOwner = new BusinessOwner
            {
                Business_ID = Id,
                Owner_Email = email
            };
            db.BusinessOwners.InsertOnSubmit(businessOwner);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public Boolean checkIfBusinessExist(int Id)
        {
            var idquery = from business in db.Businesses
                          where business.ID == Id
                          select business;
            return (idquery.Count()> 0);
        }

        public Boolean addUserLogin(string email, string password)
        {
            UsersLogIn logIn = new UsersLogIn
            {
                Email = email,
                Password = password
            };
            db.UsersLogIns.InsertOnSubmit(logIn);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public Boolean addNewUser(string email, string userName, int phone, char type)
        {
            User usr = new User
            {
                Email = email,
                UserName = userName,
                Phone = phone,
                Type = type
            };
            db.Users.InsertOnSubmit(usr);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public DisplayedCoupon GetDisplayedCoupon(int couponId)
        {
            var searchCoupon = from coupon in db.Coupons
                               join business in db.Businesses on coupon.Business_ID equals business.ID
                               where coupon.ID == couponId
                               select new DisplayedCoupon()
                               {
                                   Coupon_Name = coupon.Name,
                                   Coupon_ID = coupon.ID,
                                   Category = coupon.Category,
                                   Business_Name = business.Name,
                                   Description = coupon.Description,
                                   Original_Price = coupon.Original_Price,
                                   Discount_Price = coupon.Discount_Price,
                                   Expired_Date = coupon.Expired_Date
                               };
            return searchCoupon.ToList().First();
        }

        public int TotalOrdersOfCoupon(int couponId)
        {
            var ordersOfTheCoupon = from order in db.Orders
                                    where order.Coupon_ID == couponId
                                    select order;
            return ordersOfTheCoupon.ToList().Count();
        }

        public Boolean AddOrder(string email, int couponId, int serialKey)
        {
            Order ord = new Order
            {
                User_Email = email,
                Price = GetCoupon(couponId).Discount_Price,
                Order_Date = DateTime.Today,
                Serial = serialKey,
                Coupon_ID = couponId,
                Status = "unused"
            };

            db.Orders.InsertOnSubmit(ord);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public void AddOrderToUser(CustomerClass user, int couponId, int serialKey)
        {
            Coupon selectedCoupon = GetCoupon(couponId);
            OrderClass orderO = new OrderClass(serialKey, DateTime.Today, "unused",
                                        new CouponClass(selectedCoupon.ID, selectedCoupon.Name,
                                            selectedCoupon.Description, selectedCoupon.Original_Price,
                                            selectedCoupon.Discount_Price, selectedCoupon.Expired_Date,
                                            selectedCoupon.Status));
            user.AddNewOrder(orderO);
           }

        private Coupon GetCoupon(int couponId)
        {
            var selectedCoupon = from cou in db.Coupons
                                 where cou.ID == couponId
                                 select cou;
            return selectedCoupon.ToList().First();
        }

        public Boolean ChangeCoupon(int CouponID, string newName, string newCategory, string newDescription, int newDiscountPrice, DateTime newExpiredDate)
        {
            var FindCoupon = from coupon in db.Coupons
                             where coupon.ID == Convert.ToInt32(CouponID)
                             select coupon;
            Coupon c = FindCoupon.First();


            if (newName != "")         
                c.Name = newName;

            if (newCategory != "")
                c.Category = newCategory;

            if (newDescription != "")
                c.Description = newDescription;

            if (newDiscountPrice != 0)
                c.Discount_Price = newDiscountPrice;

            if (newExpiredDate.Date != DateTime.Now.Date)
                c.Expired_Date = newExpiredDate;

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public List<DisplayOrder> DisplayOrders(string email)
        {
            var orderedCoupons = from coupon in db.Coupons
                                 join business in db.Businesses on coupon.Business_ID equals business.ID
                                 join order in db.Orders on coupon.ID equals order.Coupon_ID
                                 where order.User_Email == email
                                 select new DisplayOrder()
                                 {
                                     Serial = order.Serial,
                                     Coupon_Name = coupon.Name,
                                     Category = coupon.Category,
                                     Business_Name = business.Name,
                                     Description = coupon.Description,
                                     Status = order.Status,
                                     Order_Date = order.Order_Date,
                                     Price_Paid = order.Price,
                                     Expired_Date = coupon.Expired_Date
                                 };
            return orderedCoupons.ToList();
        }

        public bool CheckIfUserHaveCategory(string category, string email)
        {
            var query = from prefrence in db.UserPrefrences
                        where prefrence.Email == email &&
                        prefrence.Prefrence == category
                        select prefrence;
            return (query.Count() > 0);
        }

        public Boolean AddPreferenceToUser(string email, string preference)
        {
            UserPrefrence pre = new UserPrefrence
            {
                Email = email,
                Prefrence = preference
            };

            db.UserPrefrences.InsertOnSubmit(pre);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public bool DeletePreference(string email, string preference)
        {
            var query = from prefrence in db.UserPrefrences
                        where prefrence.Email == email &&
                        prefrence.Prefrence == preference
                        select prefrence;
            UserPrefrence pre = query.ToList().First();
            db.UserPrefrences.DeleteOnSubmit(pre);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public List<DisplayRatedCoupons> ToRateCoupons(string email)
        {
            var usedCoupons = from coupon in db.Coupons
                              join business in db.Businesses on coupon.Business_ID equals business.ID
                              join order in db.Orders on coupon.ID equals order.Coupon_ID
                              where order.User_Email == email && order.Status == "used" && !db.Ratings.Any(r => r.Coupon_ID == order.Coupon_ID)
                              select new DisplayRatedCoupons()
                              {
                                  Coupon_Name = coupon.Name,
                                  Coupon_ID = coupon.ID,
                                  Category = coupon.Category,
                                  Business_Name = business.Name,
                                  Description = coupon.Description,
                                  Order_Date = order.Order_Date
                              };
            return usedCoupons.ToList();
        }

        public bool RateCoupon(string email, int couponID, int rating)
        {
            Rating rate = new Rating
            {
                Email = email,
                Coupon_ID = couponID,
                Rating1 = rating
            };

            db.Ratings.InsertOnSubmit(rate);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public List<DisplayOrderedCoupons> DisplayCouponOrders(string email)
        {
             var orderedCoupons = from order in db.Orders
                                 join coupon in db.Coupons on order.Coupon_ID equals coupon.ID
                                 join ownerr in db.BusinessOwners on coupon.Business_ID equals ownerr.Business_ID
                                 where ownerr.Owner_Email == email
                                 select new DisplayOrderedCoupons()
                                 {
                                     Business_ID = ownerr.Business_ID,
                                     Coupon_ID = coupon.ID,
                                     Coupon_Name = coupon.Name,
                                     User_Email = order.User_Email,
                                     Order_SerialKey = order.Serial,
                                     Order_Date = order.Order_Date,
                                     Order_Price_Paid = order.Price,
                                     Order_Status = order.Status
                                 };
             return orderedCoupons.ToList();
        }

        public int generateCouponID()
        {
            return (from coupon in db.Coupons
                            select coupon).Count() + 1;
        }


        public bool AddCoupon(int couponID, int businessId, int originalPrice, int discountPrice, string name, string description, string category, DateTime expiredDate)
        {
            Coupon couponToAdd = new Coupon
            {
                Business_ID = businessId,
                ID = couponID,
                Status = "notactive",
                Original_Price = originalPrice,
                Name = name,
                Description = description,
                Category = category,
                Discount_Price = discountPrice,
                Expired_Date = expiredDate
            };

            db.Coupons.InsertOnSubmit(couponToAdd);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public bool UpdateCoustomerUsedCoupon(int serialKey, string email)
        {
            Order order = (from ordr in db.Orders
                           join coupon in db.Coupons on ordr.Coupon_ID equals coupon.ID
                           join bOwner in db.BusinessOwners on coupon.Business_ID equals bOwner.Business_ID
                           where ordr.Serial == serialKey && ordr.Status == "unused" &&
                                 bOwner.Owner_Email == email
                           select ordr).First();

            if (order != null)
            {
                order.Status = "used";
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                return true;
            }
            else return false;
        }

        public List<DisplayRatedCoupon> DisplayRatedCoupons(string email)
        {
            var allCoupons = from coupon in db.Coupons
                             join ownerr in db.BusinessOwners on coupon.Business_ID equals ownerr.Business_ID
                             where ownerr.Owner_Email == email && coupon.Status == "active"
                             select new DisplayRatedCoupon()
                             {
                                 Business_ID = coupon.Business_ID,
                                 Coupon_ID = coupon.ID,
                                 Coupon_Name = coupon.Name,
                                 Original_Price = coupon.Original_Price,
                                 Discount_Price = coupon.Discount_Price,
                                 Category = coupon.Category,
                                 Description = coupon.Description,
                                 Expired_Date = coupon.Expired_Date
                             };
            return allCoupons.ToList();
        }

        public List<int> getRating(int couponID)
        {
            List<int> ratings = new List<int>();
            int star1 = (from ratingg in db.Ratings
                         where ratingg.Coupon_ID == couponID && ratingg.Rating1 == 1
                         select ratingg).Count();
            int star2 = (from ratingg in db.Ratings
                         where ratingg.Coupon_ID == couponID && ratingg.Rating1 == 2
                         select ratingg).Count();
            int star3 = (from ratingg in db.Ratings
                         where ratingg.Coupon_ID == couponID && ratingg.Rating1 == 3
                         select ratingg).Count();
            int star4 = (from ratingg in db.Ratings
                         where ratingg.Coupon_ID == couponID && ratingg.Rating1 == 4
                         select ratingg).Count();
            int star5 = (from ratingg in db.Ratings
                         where ratingg.Coupon_ID == couponID && ratingg.Rating1 == 5
                         select ratingg).Count();
            ratings.Add(star1);
            ratings.Add(star2);
            ratings.Add(star3);
            ratings.Add(star4);
            ratings.Add(star5);
            return ratings;
        }

        public bool AddSocialCoupon(string description, string link)
        {
            SocialCoupon couponToAdd = new SocialCoupon
            {
                description = description,
                Link = link
            };
            db.SocialCoupons.InsertOnSubmit(couponToAdd);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return true;
        }

        public List<DisplaySocialCoupon> DisplaySocialCoupons()
        {
            var allCoupons = from S in db.SocialCoupons
                             select new DisplaySocialCoupon() {
                             link = S.Link,
                             description = S.description
                             };
            return allCoupons.ToList();          
                             
        }
    }
}