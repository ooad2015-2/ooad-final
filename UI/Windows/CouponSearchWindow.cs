﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class CouponSearchWindow : Form
    {

        UserClass user;
        Windows.Messages.MessageHandler MH;

        public CouponSearchWindow(UserClass user, Windows.Messages.MessageHandler MH)
        {
            this.user = user;
            this.MH = MH;
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            
            string city = CityBox.Text;
            string category = CategoryBox.Text;
            BindingSource bindingSource1 = new BindingSource();             
            bindingSource1.DataSource = MH.DisplayCoupons(category, city);
            SearchCouponGrid.DataSource = bindingSource1;            
            
        }

        private void SearchCouponGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (user.GetType() == typeof(CustomerClass))
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = SearchCouponGrid.Rows[rowIndex];
                BuyCouponWindow buycouponWindow = new BuyCouponWindow((CustomerClass)user, row.Cells[1].Value.ToString(), MH);
                buycouponWindow.Show();
            }
            else if (user.GetType() == typeof(AdminClass))
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = SearchCouponGrid.Rows[rowIndex];
                EditCouponWindow editCouponWindow = new EditCouponWindow((AdminClass)user, row.Cells[1].Value.ToString(), MH);
                editCouponWindow.Show();
            }
            
        }
    }
}
