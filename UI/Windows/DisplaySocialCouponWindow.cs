﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class DisplaySocialCouponWindow : Form
    {
        Windows.Messages.MessageHandler MH;

        public DisplaySocialCouponWindow(Windows.Messages.MessageHandler mh)
        {
            this.MH = mh;
            InitializeComponent();
            initializeGrid();
        }

        public void initializeGrid()
        {
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = MH.DisplaySocialCoupons();
            SocialCouponGrid.DataSource = bindingSource1;
        }
    }
}
