﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class CouponToApproveWindow : Form
    {
        Windows.Messages.MessageHandler MH;

        public CouponToApproveWindow(Windows.Messages.MessageHandler MH)
        {
            this.MH = MH;
            InitializeComponent();
            setCouponsToApproveGrid();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ApproveOrDeleteCouponWindow appOrDel = new ApproveOrDeleteCouponWindow(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString()), this, MH);
            appOrDel.Show();
        }

        public void setCouponsToApproveGrid()
        {           
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = MH.CouponsToApprove();
            dataGridView1.DataSource = bindingSource1;             
        }
    }
}
