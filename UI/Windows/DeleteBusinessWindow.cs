﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;


namespace Windows
{
    public partial class DeleteBusinessWindow : Form
    {
        AdminClass Admin;
        string BusinessID;
        Windows.Messages.MessageHandler MH;

        public DeleteBusinessWindow(AdminClass Admin, string BusinessID, Windows.Messages.MessageHandler MH)
        {
            this.Admin = Admin;
            this.BusinessID = BusinessID;
            this.MH = MH;

            InitializeComponent();
            initializeGrid();
           
        }

        public void initializeGrid()
        {
            int businessIDInt = Convert.ToInt32(BusinessID);
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = MH.GetBusiness(businessIDInt);
            dataGridView1.DataSource = bindingSource1;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (MH.DeleteBusiness(Convert.ToInt32(BusinessID)))
            {
                MessageBox.Show("Business deleted!");
                this.Close();
            }
            else
            {
                MessageBox.Show("failed to delete business, try again!");
            }           
        }
    }
}
