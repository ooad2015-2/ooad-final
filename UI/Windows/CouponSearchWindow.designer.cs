﻿namespace Windows
{
    partial class CouponSearchWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CityBox = new System.Windows.Forms.ComboBox();
            this.CategoryBox = new System.Windows.Forms.ComboBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SearchCouponGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.SearchCouponGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // CityBox
            // 
            this.CityBox.FormattingEnabled = true;
            this.CityBox.Items.AddRange(new object[] {
            "",
            "Ashdod",
            "AshKelon",
            "Beer Sheva",
            "Eilat",
            "Hadera",
            "Haifa",
            "Jerusalem",
            "Netanya",
            "Petah Tikva",
            "Rishon LeTzion",
            "Tel Aviv"});
            this.CityBox.Location = new System.Drawing.Point(12, 51);
            this.CityBox.Name = "CityBox";
            this.CityBox.Size = new System.Drawing.Size(123, 21);
            this.CityBox.TabIndex = 0;
            // 
            // CategoryBox
            // 
            this.CategoryBox.FormattingEnabled = true;
            this.CategoryBox.Items.AddRange(new object[] {
            "",
            "Babies, children and toys",
            "Car and Home Improvement",
            "Electronics and Computers",
            "Entertainment",
            "Food and beverage",
            "Health & Beauty",
            "House and Garden",
            "Household Cleaning",
            "Men",
            "Recreation and tourism",
            "Performances",
            "Sports and outdoor",
            "Watches and Jewelry",
            "Women"});
            this.CategoryBox.Location = new System.Drawing.Point(159, 51);
            this.CategoryBox.Name = "CategoryBox";
            this.CategoryBox.Size = new System.Drawing.Size(127, 21);
            this.CategoryBox.TabIndex = 1;
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(320, 49);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(75, 23);
            this.SearchButton.TabIndex = 2;
            this.SearchButton.Text = "Search!";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "City";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(156, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Category";
            // 
            // SearchCouponGrid
            // 
            this.SearchCouponGrid.AllowUserToAddRows = false;
            this.SearchCouponGrid.AllowUserToDeleteRows = false;
            this.SearchCouponGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.SearchCouponGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.SearchCouponGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SearchCouponGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SearchCouponGrid.GridColor = System.Drawing.SystemColors.ButtonShadow;
            this.SearchCouponGrid.Location = new System.Drawing.Point(15, 96);
            this.SearchCouponGrid.Name = "SearchCouponGrid";
            this.SearchCouponGrid.ReadOnly = true;
            this.SearchCouponGrid.Size = new System.Drawing.Size(380, 152);
            this.SearchCouponGrid.TabIndex = 5;
            this.SearchCouponGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SearchCouponGrid_CellClick);
            // 
            // CouponSearchWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(424, 260);
            this.Controls.Add(this.SearchCouponGrid);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.CategoryBox);
            this.Controls.Add(this.CityBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CouponSearchWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Coupon Search";
            ((System.ComponentModel.ISupportInitialize)(this.SearchCouponGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CityBox;
        private System.Windows.Forms.ComboBox CategoryBox;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView SearchCouponGrid;
    }
}