﻿namespace Windows
{
    partial class AddBusinessOwnerWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddBusineesOwnerButton = new System.Windows.Forms.Button();
            this.ownerEmailTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OwnerPasswordTextBox = new System.Windows.Forms.TextBox();
            this.OwnerUserNameTextBox = new System.Windows.Forms.TextBox();
            this.ownerBusinessIdTextBox = new System.Windows.Forms.TextBox();
            this.OwnerPhoneTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AddBusineesOwnerButton
            // 
            this.AddBusineesOwnerButton.Location = new System.Drawing.Point(246, 184);
            this.AddBusineesOwnerButton.Name = "AddBusineesOwnerButton";
            this.AddBusineesOwnerButton.Size = new System.Drawing.Size(103, 54);
            this.AddBusineesOwnerButton.TabIndex = 0;
            this.AddBusineesOwnerButton.Text = "Add Business Owner!";
            this.AddBusineesOwnerButton.UseVisualStyleBackColor = true;
            this.AddBusineesOwnerButton.Click += new System.EventHandler(this.AddBusineesOwnerButton_Click);
            // 
            // ownerEmailTextBox
            // 
            this.ownerEmailTextBox.Location = new System.Drawing.Point(22, 75);
            this.ownerEmailTextBox.Name = "ownerEmailTextBox";
            this.ownerEmailTextBox.Size = new System.Drawing.Size(100, 20);
            this.ownerEmailTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Owner Email";
            // 
            // OwnerPasswordTextBox
            // 
            this.OwnerPasswordTextBox.Location = new System.Drawing.Point(243, 120);
            this.OwnerPasswordTextBox.Name = "OwnerPasswordTextBox";
            this.OwnerPasswordTextBox.Size = new System.Drawing.Size(100, 20);
            this.OwnerPasswordTextBox.TabIndex = 3;
            // 
            // OwnerUserNameTextBox
            // 
            this.OwnerUserNameTextBox.Location = new System.Drawing.Point(22, 169);
            this.OwnerUserNameTextBox.Name = "OwnerUserNameTextBox";
            this.OwnerUserNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.OwnerUserNameTextBox.TabIndex = 4;
            // 
            // ownerBusinessIdTextBox
            // 
            this.ownerBusinessIdTextBox.Location = new System.Drawing.Point(22, 120);
            this.ownerBusinessIdTextBox.Name = "ownerBusinessIdTextBox";
            this.ownerBusinessIdTextBox.Size = new System.Drawing.Size(100, 20);
            this.ownerBusinessIdTextBox.TabIndex = 5;
            // 
            // OwnerPhoneTextBox
            // 
            this.OwnerPhoneTextBox.Location = new System.Drawing.Point(243, 75);
            this.OwnerPhoneTextBox.Name = "OwnerPhoneTextBox";
            this.OwnerPhoneTextBox.Size = new System.Drawing.Size(100, 20);
            this.OwnerPhoneTextBox.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Business ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(243, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Phone";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(243, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "User Name";
            // 
            // AddBusinessOwnerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 260);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.OwnerPhoneTextBox);
            this.Controls.Add(this.ownerBusinessIdTextBox);
            this.Controls.Add(this.OwnerUserNameTextBox);
            this.Controls.Add(this.OwnerPasswordTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ownerEmailTextBox);
            this.Controls.Add(this.AddBusineesOwnerButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddBusinessOwnerWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Business Owner";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddBusineesOwnerButton;
        private System.Windows.Forms.TextBox ownerEmailTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox OwnerPasswordTextBox;
        private System.Windows.Forms.TextBox OwnerUserNameTextBox;
        private System.Windows.Forms.TextBox ownerBusinessIdTextBox;
        private System.Windows.Forms.TextBox OwnerPhoneTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}