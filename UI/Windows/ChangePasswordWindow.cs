﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class ChangePasswordWindow : Form
    {
        UserClass user;
        Windows.Messages.MessageHandler MH;

        public ChangePasswordWindow(UserClass user,Windows.Messages.MessageHandler MH)
        {
            this.user = user;
            this.MH = MH;
            InitializeComponent();
        }

        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            string currentPassword = CurrentPasswordBox.Text;
            string newPassword = NewPasswordBox.Text;

            string msg = MH.UpdatePassword(user.getEmail(), currentPassword, newPassword);
            MessageBox.Show(msg);
            if (msg == "Password changed successfully")
            {
                this.Close();
            }      
        }
    }
}
