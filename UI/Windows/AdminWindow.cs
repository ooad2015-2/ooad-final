﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class AdminWindow : Form
    {
        AdminClass admin;
        Windows.Messages.MessageHandler MH;

        public AdminWindow(AdminClass admin, Windows.Messages.MessageHandler mh)
        {
            this.admin = admin;
            this.MH = mh;
            InitializeComponent();
        }

        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            ChangePasswordWindow changePasswordWindow = new ChangePasswordWindow(admin,MH);
            changePasswordWindow.Show();
        }

        private void CouponListButton_Click(object sender, EventArgs e)
        {
            CouponSearchWindow couponSearchWindow = new CouponSearchWindow(admin,MH);
            couponSearchWindow.Show();
        }

        private void BusinessesListButton_Click(object sender, EventArgs e)
        {
            BusinessSearchWindow businessSearchWindow = new BusinessSearchWindow(admin,MH);
            businessSearchWindow.Show();
        }

        private void CouponsApprovalButton_Click(object sender, EventArgs e)
        {
            CouponToApproveWindow couponToApprove = new CouponToApproveWindow(MH);
            couponToApprove.Show();
        }

        private void AddNewBusinessButton_Click(object sender, EventArgs e)
        {
            AddBusinessWindow addBusinessWindow = new AddBusinessWindow(MH);
            addBusinessWindow.Show();
        }

        private void AddNewBusinessOwnerButton_Click(object sender, EventArgs e)
        {
            AddBusinessOwnerWindow addBusinessOwnerWindow = new AddBusinessOwnerWindow(MH);
            addBusinessOwnerWindow.Show();
        }

        private void LogOutButton_Click(object sender, EventArgs e)
        {
            MainWindow main = new MainWindow(MH);
            main.Show();
            this.Close();
        }

        private void AdminWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainWindow main = new MainWindow(MH);
            main.Show();
        }
    }
}
