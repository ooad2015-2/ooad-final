﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class RateCouponsWindow : Form
    {
        CustomerClass customer;
        Windows.Messages.MessageHandler MH;

        public RateCouponsWindow(CustomerClass customer, Windows.Messages.MessageHandler mh)
        {
            this.customer = customer;
            this.MH = mh;
            InitializeComponent();
            setCouponsToRate();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            Rate1_5 rate = new Rate1_5(customer, Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString()),this, MH);
            rate.Show();

        }
        public void setCouponsToRate()
        {           
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = MH.ToRateCoupons(customer.getEmail());
            dataGridView1.DataSource = bindingSource1;
        }
    }
}
