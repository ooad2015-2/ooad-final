﻿namespace Windows
{
    partial class AddNewCouponByOwnerWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BusinessIDTextBox = new System.Windows.Forms.TextBox();
            this.CategoryComboBox = new System.Windows.Forms.ComboBox();
            this.AddNewCouponButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.couponNamtextBox = new System.Windows.Forms.TextBox();
            this.OriginalPriceTextBox = new System.Windows.Forms.TextBox();
            this.DiscountPriceTextBox = new System.Windows.Forms.TextBox();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ExpiredDatePicker = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // BusinessIDTextBox
            // 
            this.BusinessIDTextBox.Location = new System.Drawing.Point(29, 55);
            this.BusinessIDTextBox.Name = "BusinessIDTextBox";
            this.BusinessIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.BusinessIDTextBox.TabIndex = 0;
            // 
            // CategoryComboBox
            // 
            this.CategoryComboBox.FormattingEnabled = true;
            this.CategoryComboBox.Items.AddRange(new object[] {
            "Babies, children and toys",
            "Car and Home Improvement",
            "Electronics and Computers",
            "Entertainment",
            "Food and beverage",
            "Health & Beauty",
            "House and Garden",
            "Household Cleaning",
            "Men",
            "Recreation and tourism",
            "Performances",
            "Sports and outdoor",
            "Watches and Jewelry",
            "Women"});
            this.CategoryComboBox.Location = new System.Drawing.Point(297, 127);
            this.CategoryComboBox.Name = "CategoryComboBox";
            this.CategoryComboBox.Size = new System.Drawing.Size(100, 21);
            this.CategoryComboBox.TabIndex = 1;
            // 
            // AddNewCouponButton
            // 
            this.AddNewCouponButton.Location = new System.Drawing.Point(160, 192);
            this.AddNewCouponButton.Name = "AddNewCouponButton";
            this.AddNewCouponButton.Size = new System.Drawing.Size(237, 34);
            this.AddNewCouponButton.TabIndex = 2;
            this.AddNewCouponButton.Text = "Add New Coupon";
            this.AddNewCouponButton.UseVisualStyleBackColor = true;
            this.AddNewCouponButton.Click += new System.EventHandler(this.AddNewCouponButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Business ID";
            // 
            // couponNamtextBox
            // 
            this.couponNamtextBox.Location = new System.Drawing.Point(29, 127);
            this.couponNamtextBox.Name = "couponNamtextBox";
            this.couponNamtextBox.Size = new System.Drawing.Size(100, 20);
            this.couponNamtextBox.TabIndex = 4;
            // 
            // OriginalPriceTextBox
            // 
            this.OriginalPriceTextBox.Location = new System.Drawing.Point(160, 55);
            this.OriginalPriceTextBox.Name = "OriginalPriceTextBox";
            this.OriginalPriceTextBox.Size = new System.Drawing.Size(100, 20);
            this.OriginalPriceTextBox.TabIndex = 6;
            // 
            // DiscountPriceTextBox
            // 
            this.DiscountPriceTextBox.Location = new System.Drawing.Point(297, 55);
            this.DiscountPriceTextBox.Name = "DiscountPriceTextBox";
            this.DiscountPriceTextBox.Size = new System.Drawing.Size(100, 20);
            this.DiscountPriceTextBox.TabIndex = 7;
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.Location = new System.Drawing.Point(160, 127);
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.DescriptionTextBox.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(157, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Original Price";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(294, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Discoun Price";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(157, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Description";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(294, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Category";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Expired Date";
            // 
            // ExpiredDatePicker
            // 
            this.ExpiredDatePicker.CustomFormat = "dd-MM-yyyy";
            this.ExpiredDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ExpiredDatePicker.Location = new System.Drawing.Point(29, 206);
            this.ExpiredDatePicker.MaxDate = new System.DateTime(2020, 12, 31, 0, 0, 0, 0);
            this.ExpiredDatePicker.MinDate = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.ExpiredDatePicker.Name = "ExpiredDatePicker";
            this.ExpiredDatePicker.Size = new System.Drawing.Size(77, 20);
            this.ExpiredDatePicker.TabIndex = 15;
            this.ExpiredDatePicker.Value = new System.DateTime(2015, 6, 30, 13, 1, 53, 0);
            // 
            // AddNewCouponByOwnerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 260);
            this.Controls.Add(this.ExpiredDatePicker);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DescriptionTextBox);
            this.Controls.Add(this.DiscountPriceTextBox);
            this.Controls.Add(this.OriginalPriceTextBox);
            this.Controls.Add(this.couponNamtextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddNewCouponButton);
            this.Controls.Add(this.CategoryComboBox);
            this.Controls.Add(this.BusinessIDTextBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddNewCouponByOwnerWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Coupon";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox BusinessIDTextBox;
        private System.Windows.Forms.ComboBox CategoryComboBox;
        private System.Windows.Forms.Button AddNewCouponButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox couponNamtextBox;
        private System.Windows.Forms.TextBox OriginalPriceTextBox;
        private System.Windows.Forms.TextBox DiscountPriceTextBox;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker ExpiredDatePicker;
    }
}