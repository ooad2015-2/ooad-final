﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class UpdateCouponOfCustomerWindow : Form
    {
        BusinessOwnerClass owner;
        Windows.Messages.MessageHandler MH;

        public UpdateCouponOfCustomerWindow(BusinessOwnerClass owner, Windows.Messages.MessageHandler mh)
        {
            this.owner = owner;
            this.MH = mh;
            InitializeComponent();
        }

        private void UpdateCoustomerCouponButton_Click(object sender, EventArgs e)
        {
            int serialKey = Convert.ToInt32(SerialKeyTextBox.Text);
            if(MH.UpdateCoustomerUsedCoupon(serialKey, owner.getEmail()))
            {
                MessageBox.Show("you have updated this order!");
                this.Close();
            } 
            else {
                MessageBox.Show("this serial is not exist or already used");
            }
        }
    }
}
