﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class OwnersCouponOrderesWindow : Form
    {
        BusinessOwnerClass owner;
        Windows.Messages.MessageHandler MH;

        public OwnersCouponOrderesWindow(BusinessOwnerClass owner, Windows.Messages.MessageHandler mh)
        {
            this.owner = owner;
            this.MH = mh;
            InitializeComponent();
            initializeGrid();       
        }

        public void initializeGrid()
        {
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = MH.DisplayCouponOrders(owner.getEmail());
            dataGridView1.DataSource = bindingSource1;
        }
    }
}
