﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Windows
{
    public partial class ShowCouponRatingWindow : Form
    {
        int couponID;
        Windows.Messages.MessageHandler MH;

        public ShowCouponRatingWindow(int CouponID, Windows.Messages.MessageHandler mh)
        {
            this.couponID = CouponID;
            this.MH = mh;
            InitializeComponent();
            initializeRatings();
        }

        public void initializeRatings()
        {
            int star1 = MH.getRating(couponID).ElementAt(0);
            int star2 = MH.getRating(couponID).ElementAt(1);
            int star3 = MH.getRating(couponID).ElementAt(2);
            int star4 = MH.getRating(couponID).ElementAt(3);
            int star5 = MH.getRating(couponID).ElementAt(4);

            Star1Label.Text = Convert.ToString(star1);
            Star2Label.Text = Convert.ToString(star2);
            Star3Label.Text = Convert.ToString(star3);
            Star4Label.Text = Convert.ToString(star4);
            Star5Label.Text = Convert.ToString(star5);
            double TotalRating = 0;
            if ((star1 + star2 + star3 + star4 + star5) != 0)
            {
                TotalRating = (1 * star1 + 2 * star2 + 3 * star3 + 4 * star4 + 5 * star5) / (star1 + star2 + star3 + star4 + star5);
            }
            TotalRatingLabel.Text = Convert.ToString(TotalRating);
        }

    }
}
