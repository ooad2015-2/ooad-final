﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class Rate1_5 : Form
    {
        CustomerClass customer;
        int couponId;
        RateCouponsWindow rateCouponWindow;
        Windows.Messages.MessageHandler MH;

        public Rate1_5(CustomerClass customer, int couponID, RateCouponsWindow rateCouponWindow, Windows.Messages.MessageHandler mh)
        {
            this.customer = customer;
            this.couponId = couponID;
            this.rateCouponWindow = rateCouponWindow;
            this.MH = mh;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you have rated this coupon with - 1 stars", "");
            updateRatingInDB(1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you have rated this coupon with - 2 stars", "");
            updateRatingInDB(2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you have rated this coupon with - 3 stars", "");
            updateRatingInDB(3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you have rated this coupon with - 4 stars", "");
            updateRatingInDB(4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("you have rated this coupon with - 5 stars", "");
            updateRatingInDB(5);
        }

        private void updateRatingInDB(int rating)
        {
            rateCouponWindow.setCouponsToRate();
            if (MH.RateCoupon(customer.getEmail(), couponId, rating))
            {
                rateCouponWindow.setCouponsToRate();
                this.Close();
            }
            else
            {
                MessageBox.Show("Something went wrong, try again later");
            }
        }
    }
}
