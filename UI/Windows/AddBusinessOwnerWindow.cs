﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Windows
{
    public partial class AddBusinessOwnerWindow : Form
    {
        Windows.Messages.MessageHandler MH;

        public AddBusinessOwnerWindow(Windows.Messages.MessageHandler MH)
        {
            this.MH = MH;
            InitializeComponent();
        }

        private void AddBusineesOwnerButton_Click(object sender, EventArgs e)
        {
            int Id = Convert.ToInt32(ownerBusinessIdTextBox.Text);
            string email = ownerEmailTextBox.Text;
            string userName = OwnerUserNameTextBox.Text;
            int phone = Convert.ToInt32(OwnerPhoneTextBox.Text);
            string password = OwnerPasswordTextBox.Text;

            if (MH.AddBusinessOwner(Id, email, userName, phone, password))
            {
                MessageBox.Show("business owner added!");
                this.Close();
            }
            else
            {
                MessageBox.Show("failed to add business owner, check your inputs and try again!");
            }
        }
    }
}
