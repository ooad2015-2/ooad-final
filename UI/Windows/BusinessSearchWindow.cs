﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class BusinessSearchWindow : Form
    {
        UserClass User;
        Windows.Messages.MessageHandler MH;

        public BusinessSearchWindow(UserClass user, Windows.Messages.MessageHandler MH)
        {
            User = user;
            this.MH = MH;
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            
            string city = CityBox.Text;
            string category = CategoryBox.Text;

            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = MH.DisplayBusinesses(category, city);
            SearchBusinessGrid.DataSource = bindingSource1;
            
        }

        private void SearchBusinessGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (User.GetType() == typeof(AdminClass))
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = SearchBusinessGrid.Rows[rowIndex];
                DeleteBusinessWindow deleteBusinessWindow = new DeleteBusinessWindow((AdminClass)User, row.Cells[0].Value.ToString(), MH);
                deleteBusinessWindow.Show();
            }
        }
    }
}
