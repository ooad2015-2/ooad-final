﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class AddNewCouponByOwnerWindow : Form
    {
        BusinessOwnerClass owner;
        Windows.Messages.MessageHandler MH;

        public AddNewCouponByOwnerWindow(BusinessOwnerClass owner, Windows.Messages.MessageHandler mh)
        {
            this.owner = owner;
            this.MH = mh;
            InitializeComponent();
        }

        private void AddNewCouponButton_Click(object sender, EventArgs e)
        {
            
            int businessId = Convert.ToInt32(BusinessIDTextBox.Text);
            int originalPrice = Convert.ToInt32(OriginalPriceTextBox.Text);
            int discountPrice = Convert.ToInt32(DiscountPriceTextBox.Text);
            string name = couponNamtextBox.Text;
            string description = DescriptionTextBox.Text;
            string category = CategoryComboBox.Text;
            DateTime expiredDate = ExpiredDatePicker.Value;

            if (MH.AddCoupon(businessId, originalPrice,discountPrice, name, description, category, expiredDate)) 
            {
                MessageBox.Show("coupon added!");
                this.Close();
            }
            else
            {
                MessageBox.Show("one of your inputs is wrong");
            }
        }
    }
}
