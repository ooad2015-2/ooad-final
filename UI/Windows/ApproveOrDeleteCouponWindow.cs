﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class ApproveOrDeleteCouponWindow : Form
    {
        int couponID;
        CouponToApproveWindow couponToApproveWindow;
        Windows.Messages.MessageHandler MH;

        public ApproveOrDeleteCouponWindow(int couponID, CouponToApproveWindow couponToApproveWindow, Windows.Messages.MessageHandler MH)
        {
            this.couponToApproveWindow = couponToApproveWindow;
            this.couponID = couponID;
            this.MH = MH;

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)//approve
        {
            if (MH.ApproveCoupon(couponID))
            {
                MessageBox.Show("you have approved this coupon!");
                couponToApproveWindow.setCouponsToApproveGrid();
                this.Close();
            }
            else
            {
                MessageBox.Show("faild to approve, try again!");
            }                               
        }

        private void button2_Click(object sender, EventArgs e)//delete
        {
            if (MH.DeleteCoupon(couponID)){
                MessageBox.Show("you have deleted this coupon!", "");
                couponToApproveWindow.setCouponsToApproveGrid();
                this.Close();
            }else{
                MessageBox.Show("faild to delete, try again!", "");
            }
             
        }
    }
}
