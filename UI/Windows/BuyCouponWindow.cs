﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;
using System.Net.Mail;
using System.Net;

namespace Windows
{
    public partial class BuyCouponWindow : Form
    {
        CustomerClass customer;
        int CouponID;
        Windows.Messages.MessageHandler MH;

        public BuyCouponWindow(CustomerClass customer, string couponID, Windows.Messages.MessageHandler mh)
        {
            this.customer = customer;
            this.CouponID = Convert.ToInt32(couponID);
            this.MH = mh;

            InitializeComponent();
            initializeGrid();
           
        }

        private void initializeGrid()
        {
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = MH.GetCoupon(Convert.ToInt32(CouponID));
            dataGridView1.DataSource = bindingSource1;
        }

        private void BuyButton_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "")
            {
                if (MH.OrderCoupon(CouponID, customer))
                {
                    MessageBox.Show("you have purchased this coupon! ");
                    sendEmail();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("something went wrong, please try again! ");
                }
            }
            else
            {
                MessageBox.Show("wrong input!, please try again ");
            }              
        }

        private void sendEmail()
        {
            try{
            MailMessage message = new MailMessage();
                    SmtpClient smtp = new SmtpClient();

                    message.From = new MailAddress("couponsSystemBgu@gmail.com");
                    message.To.Add(new MailAddress(customer.getEmail()));
                    message.Subject = "Confirmation";
                    message.Body = customer.getOrdersList().Last().ToString();

                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential("couponsSystemBgu@gmail.com", "bgu123456");
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(message);
                    MessageBox.Show("an Email with confirmation and coupon details has been sent to you", "");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("err: " + ex.Message);
                }
        }
    }
}
