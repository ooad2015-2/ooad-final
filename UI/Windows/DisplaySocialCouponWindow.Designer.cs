﻿namespace UI
{
    partial class DisplaySocialCouponWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SocialCouponGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.SocialCouponGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // SocialCouponGrid
            // 
            this.SocialCouponGrid.AllowUserToAddRows = false;
            this.SocialCouponGrid.AllowUserToDeleteRows = false;
            this.SocialCouponGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.SocialCouponGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.SocialCouponGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SocialCouponGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SocialCouponGrid.GridColor = System.Drawing.SystemColors.ButtonShadow;
            this.SocialCouponGrid.Location = new System.Drawing.Point(12, 23);
            this.SocialCouponGrid.Name = "SocialCouponGrid";
            this.SocialCouponGrid.ReadOnly = true;
            this.SocialCouponGrid.Size = new System.Drawing.Size(390, 214);
            this.SocialCouponGrid.TabIndex = 6;
            // 
            // DisplaySocialCouponWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 260);
            this.Controls.Add(this.SocialCouponGrid);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DisplaySocialCouponWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DisplaySocialCouponWindow";
            ((System.ComponentModel.ISupportInitialize)(this.SocialCouponGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView SocialCouponGrid;
    }
}