﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class CustumerOrderedCouponsWindow : Form
    {
        CustomerClass customer;
        Windows.Messages.MessageHandler MH;

        public CustumerOrderedCouponsWindow(CustomerClass customer, Windows.Messages.MessageHandler MH)
        {
            this.customer = customer;
            this.MH = MH;
            InitializeComponent();
            initializeGrid();
        }

        public void initializeGrid()
        {
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = MH.DisplayOrders(customer.getEmail());
            OrderedCouponsGrid.DataSource = bindingSource1;
        }
    }
}
