﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class EditCouponWindow : Form
    {
        AdminClass Admin;
        int CouponID;
        Windows.Messages.MessageHandler MH;

        public EditCouponWindow(AdminClass Admin, String CouponID, Windows.Messages.MessageHandler mh)
        {
            this.Admin = Admin;
            this.CouponID = Convert.ToInt32(CouponID);
            this.MH = mh;
            InitializeComponent();
            initializeGrid();          
        }

        public void initializeGrid()
        {
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = MH.DisplayCoupons(CouponID);
            dataGridView1.DataSource = bindingSource1;
        }
        private void ChangeCouponButton_Click(object sender, EventArgs e)
        {
            string newName = CouponNameTextBox.Text;
            string newCategory = CategoryBox.Text;
            string newDescription = DescriptiontextBox.Text;
            int newDiscountPrice = Convert.ToInt32(DiscountTextBox.Text);
            DateTime newExpiredDate = ExpireDatePicker.Value.Date;

            if (MH.ChangeCoupon(CouponID, newName, newCategory, newDescription, newDiscountPrice, newExpiredDate))
            {
                MessageBox.Show("changed!");
                this.Close();
            }
            else
            {
                MessageBox.Show("wrong details!, try again");
            }
          }
    }
}
