﻿namespace Windows
{
    partial class BusinessOwnerWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CouponsSearchButton = new System.Windows.Forms.Button();
            this.OrderedCouponsButton = new System.Windows.Forms.Button();
            this.AddNewCouponButton = new System.Windows.Forms.Button();
            this.CouponsRatingsButton = new System.Windows.Forms.Button();
            this.UpdateExploitedCouponButton = new System.Windows.Forms.Button();
            this.ChangePasswordButton = new System.Windows.Forms.Button();
            this.LogOutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CouponsSearchButton
            // 
            this.CouponsSearchButton.Location = new System.Drawing.Point(25, 70);
            this.CouponsSearchButton.Name = "CouponsSearchButton";
            this.CouponsSearchButton.Size = new System.Drawing.Size(153, 23);
            this.CouponsSearchButton.TabIndex = 0;
            this.CouponsSearchButton.Text = "Cupons Search";
            this.CouponsSearchButton.UseVisualStyleBackColor = true;
            this.CouponsSearchButton.Click += new System.EventHandler(this.CouponsSearchButton_Click);
            // 
            // OrderedCouponsButton
            // 
            this.OrderedCouponsButton.Location = new System.Drawing.Point(25, 127);
            this.OrderedCouponsButton.Name = "OrderedCouponsButton";
            this.OrderedCouponsButton.Size = new System.Drawing.Size(153, 23);
            this.OrderedCouponsButton.TabIndex = 1;
            this.OrderedCouponsButton.Text = "Ordered Coupons";
            this.OrderedCouponsButton.UseVisualStyleBackColor = true;
            this.OrderedCouponsButton.Click += new System.EventHandler(this.OrderedCouponsButton_Click);
            // 
            // AddNewCouponButton
            // 
            this.AddNewCouponButton.Location = new System.Drawing.Point(25, 180);
            this.AddNewCouponButton.Name = "AddNewCouponButton";
            this.AddNewCouponButton.Size = new System.Drawing.Size(153, 23);
            this.AddNewCouponButton.TabIndex = 2;
            this.AddNewCouponButton.Text = "Add New Coupon";
            this.AddNewCouponButton.UseVisualStyleBackColor = true;
            this.AddNewCouponButton.Click += new System.EventHandler(this.AddNewCouponButton_Click);
            // 
            // CouponsRatingsButton
            // 
            this.CouponsRatingsButton.Location = new System.Drawing.Point(228, 70);
            this.CouponsRatingsButton.Name = "CouponsRatingsButton";
            this.CouponsRatingsButton.Size = new System.Drawing.Size(153, 23);
            this.CouponsRatingsButton.TabIndex = 3;
            this.CouponsRatingsButton.Text = "Coupons Rating";
            this.CouponsRatingsButton.UseVisualStyleBackColor = true;
            this.CouponsRatingsButton.Click += new System.EventHandler(this.CouponsRatingsButton_Click);
            // 
            // UpdateExploitedCouponButton
            // 
            this.UpdateExploitedCouponButton.Location = new System.Drawing.Point(228, 127);
            this.UpdateExploitedCouponButton.Name = "UpdateExploitedCouponButton";
            this.UpdateExploitedCouponButton.Size = new System.Drawing.Size(153, 23);
            this.UpdateExploitedCouponButton.TabIndex = 4;
            this.UpdateExploitedCouponButton.Text = "Update Exploited Coupon";
            this.UpdateExploitedCouponButton.UseVisualStyleBackColor = true;
            this.UpdateExploitedCouponButton.Click += new System.EventHandler(this.UpdateExploitedCouponButton_Click);
            // 
            // ChangePasswordButton
            // 
            this.ChangePasswordButton.Location = new System.Drawing.Point(228, 180);
            this.ChangePasswordButton.Name = "ChangePasswordButton";
            this.ChangePasswordButton.Size = new System.Drawing.Size(153, 23);
            this.ChangePasswordButton.TabIndex = 5;
            this.ChangePasswordButton.Text = "Change Password";
            this.ChangePasswordButton.UseVisualStyleBackColor = true;
            this.ChangePasswordButton.Click += new System.EventHandler(this.ChangePasswordButton_Click);
            // 
            // LogOutButton
            // 
            this.LogOutButton.Location = new System.Drawing.Point(25, 225);
            this.LogOutButton.Name = "LogOutButton";
            this.LogOutButton.Size = new System.Drawing.Size(61, 23);
            this.LogOutButton.TabIndex = 8;
            this.LogOutButton.Text = "Log Out";
            this.LogOutButton.UseVisualStyleBackColor = true;
            this.LogOutButton.Click += new System.EventHandler(this.LogOutButton_Click);
            // 
            // BusinessOwnerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 260);
            this.Controls.Add(this.LogOutButton);
            this.Controls.Add(this.ChangePasswordButton);
            this.Controls.Add(this.UpdateExploitedCouponButton);
            this.Controls.Add(this.CouponsRatingsButton);
            this.Controls.Add(this.AddNewCouponButton);
            this.Controls.Add(this.OrderedCouponsButton);
            this.Controls.Add(this.CouponsSearchButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BusinessOwnerWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grouponim";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BusinessOwnerWindow_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CouponsSearchButton;
        private System.Windows.Forms.Button OrderedCouponsButton;
        private System.Windows.Forms.Button AddNewCouponButton;
        private System.Windows.Forms.Button CouponsRatingsButton;
        private System.Windows.Forms.Button UpdateExploitedCouponButton;
        private System.Windows.Forms.Button ChangePasswordButton;
        private System.Windows.Forms.Button LogOutButton;
    }
}