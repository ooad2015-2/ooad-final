﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Windows 
{
    public partial class AddCouponFromSocialWindow : Form
    {
         Windows.Messages.MessageHandler MH;
        public AddCouponFromSocialWindow(Windows.Messages.MessageHandler mh)
        {
            this.MH = mh;
            InitializeComponent();
        }
         
        private void addSocialCoupon_Click(object sender, EventArgs e)
        {
            string description = DescriptionText.Text;
            string link = LinkTextBox.Text;
            if (MH.AddSocialCoupon(description, link))
            {
                MessageBox.Show("coupon added!");
                this.Close();
            }
            else
            {
                MessageBox.Show("faild!, try again");
            }
        }
    }
}
