﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class BusinessOwnerWindow : Form
    {
        BusinessOwnerClass owner;
        Windows.Messages.MessageHandler MH;

        public BusinessOwnerWindow(BusinessOwnerClass owner, Windows.Messages.MessageHandler mh)
        {
            this.owner = owner;
            this.MH = mh;
            InitializeComponent();
        }

        private void CouponsSearchButton_Click(object sender, EventArgs e)
        {
            CouponSearchWindow couponSearchWindow = new CouponSearchWindow(owner,MH);
            couponSearchWindow.Show();
        }

        private void OrderedCouponsButton_Click(object sender, EventArgs e)
        {
            OwnersCouponOrderesWindow orderedCouponsWindow = new OwnersCouponOrderesWindow(owner, MH);
            orderedCouponsWindow.Show();
        }

        private void AddNewCouponButton_Click(object sender, EventArgs e)
        {
            AddNewCouponByOwnerWindow addCouponWindow = new AddNewCouponByOwnerWindow(owner, MH);
            addCouponWindow.Show();
        }

        private void CouponsRatingsButton_Click(object sender, EventArgs e)
        {
            OwnersCouponsRatingWindow couponRatingWindow = new OwnersCouponsRatingWindow(owner, MH);
            couponRatingWindow.Show();
        }

        private void UpdateExploitedCouponButton_Click(object sender, EventArgs e)
        {
            UpdateCouponOfCustomerWindow updateWindow = new UpdateCouponOfCustomerWindow(owner, MH);
            updateWindow.Show();
        }

        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            ChangePasswordWindow changePasswordWindow = new ChangePasswordWindow(owner,MH);
            changePasswordWindow.Show();
        }

        private void LogOutButton_Click(object sender, EventArgs e)
        {
            MainWindow main = new MainWindow(MH);
            main.Show();
            this.Close();
        }

        private void BusinessOwnerWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainWindow main = new MainWindow(MH);
            main.Show();
        }
    }
}
