﻿namespace Windows
{
    partial class UpdateCouponOfCustomerWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SerialKeyTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.UpdateCoustomerCouponButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SerialKeyTextBox
            // 
            this.SerialKeyTextBox.Location = new System.Drawing.Point(80, 66);
            this.SerialKeyTextBox.Name = "SerialKeyTextBox";
            this.SerialKeyTextBox.Size = new System.Drawing.Size(153, 20);
            this.SerialKeyTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(44, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter the serial key of the order:";
            // 
            // UpdateCoustomerCouponButton
            // 
            this.UpdateCoustomerCouponButton.Location = new System.Drawing.Point(108, 119);
            this.UpdateCoustomerCouponButton.Name = "UpdateCoustomerCouponButton";
            this.UpdateCoustomerCouponButton.Size = new System.Drawing.Size(96, 30);
            this.UpdateCoustomerCouponButton.TabIndex = 2;
            this.UpdateCoustomerCouponButton.Text = "Update!";
            this.UpdateCoustomerCouponButton.UseVisualStyleBackColor = true;
            this.UpdateCoustomerCouponButton.Click += new System.EventHandler(this.UpdateCoustomerCouponButton_Click);
            // 
            // UpdateCouponOfCustomerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 182);
            this.Controls.Add(this.UpdateCoustomerCouponButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SerialKeyTextBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateCouponOfCustomerWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Coupon Of Customer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox SerialKeyTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button UpdateCoustomerCouponButton;
    }
}