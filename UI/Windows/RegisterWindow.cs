﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Windows
{
    public partial class RegisterWindow : Form
    {
        Windows.Messages.MessageHandler MH;

        public RegisterWindow(Windows.Messages.MessageHandler mh)
        {
            this.MH = mh;
            InitializeComponent();
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            int phone = Convert.ToInt32(PhoneTextBox.Text);
            string email = EmailTextBox.Text;
            string userName = UserNameTextBox.Text;
            string password = PasswordTextBox.Text;

            List<string> prefrences = new List<string>();
                foreach (object itemChecked in CategoryListBox.CheckedItems)
                {
                   prefrences.Add(itemChecked.ToString());
                }
              
                if (MH.RegisterCustumer(email, password,userName,phone,prefrences))
                {
                    MessageBox.Show("You have successfully registered");
                }
                else
                {
                    MessageBox.Show("E-mail is already exist in the system", "Wrong Input");
                }
        }

    }
}
