﻿namespace Windows
{
    partial class CustumerWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SearchCouponsButton = new System.Windows.Forms.Button();
            this.BusinessSearchButton = new System.Windows.Forms.Button();
            this.RateCouponsButton = new System.Windows.Forms.Button();
            this.ChangePasswordButton = new System.Windows.Forms.Button();
            this.UpdateCategoriesButton = new System.Windows.Forms.Button();
            this.addFromSocialNetButton = new System.Windows.Forms.Button();
            this.OrderedCouponsButton = new System.Windows.Forms.Button();
            this.LogOutButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SocialCouponDisplabutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SearchCouponsButton
            // 
            this.SearchCouponsButton.Location = new System.Drawing.Point(12, 53);
            this.SearchCouponsButton.Name = "SearchCouponsButton";
            this.SearchCouponsButton.Size = new System.Drawing.Size(183, 23);
            this.SearchCouponsButton.TabIndex = 0;
            this.SearchCouponsButton.Text = "Search for Coupons";
            this.SearchCouponsButton.UseVisualStyleBackColor = true;
            this.SearchCouponsButton.Click += new System.EventHandler(this.SearchCouponsButton_Click);
            // 
            // BusinessSearchButton
            // 
            this.BusinessSearchButton.Location = new System.Drawing.Point(12, 104);
            this.BusinessSearchButton.Name = "BusinessSearchButton";
            this.BusinessSearchButton.Size = new System.Drawing.Size(183, 23);
            this.BusinessSearchButton.TabIndex = 1;
            this.BusinessSearchButton.Text = "Search for Business";
            this.BusinessSearchButton.UseVisualStyleBackColor = true;
            this.BusinessSearchButton.Click += new System.EventHandler(this.BusinessSearchButton_Click);
            // 
            // RateCouponsButton
            // 
            this.RateCouponsButton.Location = new System.Drawing.Point(12, 156);
            this.RateCouponsButton.Name = "RateCouponsButton";
            this.RateCouponsButton.Size = new System.Drawing.Size(183, 23);
            this.RateCouponsButton.TabIndex = 2;
            this.RateCouponsButton.Text = "Rate Coupons";
            this.RateCouponsButton.UseVisualStyleBackColor = true;
            this.RateCouponsButton.Click += new System.EventHandler(this.RateCouponsButton_Click);
            // 
            // ChangePasswordButton
            // 
            this.ChangePasswordButton.Location = new System.Drawing.Point(43, 196);
            this.ChangePasswordButton.Name = "ChangePasswordButton";
            this.ChangePasswordButton.Size = new System.Drawing.Size(109, 23);
            this.ChangePasswordButton.TabIndex = 3;
            this.ChangePasswordButton.Text = "Change Password";
            this.ChangePasswordButton.UseVisualStyleBackColor = true;
            this.ChangePasswordButton.Click += new System.EventHandler(this.ChangePasswordButton_Click);
            // 
            // UpdateCategoriesButton
            // 
            this.UpdateCategoriesButton.Location = new System.Drawing.Point(228, 53);
            this.UpdateCategoriesButton.Name = "UpdateCategoriesButton";
            this.UpdateCategoriesButton.Size = new System.Drawing.Size(183, 23);
            this.UpdateCategoriesButton.TabIndex = 4;
            this.UpdateCategoriesButton.Text = "Update Favorite Categories";
            this.UpdateCategoriesButton.UseVisualStyleBackColor = true;
            this.UpdateCategoriesButton.Click += new System.EventHandler(this.UpdateCategoriesButton_Click);
            // 
            // addFromSocialNetButton
            // 
            this.addFromSocialNetButton.Location = new System.Drawing.Point(228, 104);
            this.addFromSocialNetButton.Name = "addFromSocialNetButton";
            this.addFromSocialNetButton.Size = new System.Drawing.Size(183, 23);
            this.addFromSocialNetButton.TabIndex = 5;
            this.addFromSocialNetButton.Text = "Add Coupon from Social Network";
            this.addFromSocialNetButton.UseVisualStyleBackColor = true;
            this.addFromSocialNetButton.Click += new System.EventHandler(this.addFromSocialNetButton_Click);
            // 
            // OrderedCouponsButton
            // 
            this.OrderedCouponsButton.Location = new System.Drawing.Point(228, 156);
            this.OrderedCouponsButton.Name = "OrderedCouponsButton";
            this.OrderedCouponsButton.Size = new System.Drawing.Size(183, 23);
            this.OrderedCouponsButton.TabIndex = 6;
            this.OrderedCouponsButton.Text = "Ordered Coupons";
            this.OrderedCouponsButton.UseVisualStyleBackColor = true;
            this.OrderedCouponsButton.Click += new System.EventHandler(this.OrderedCouponsButton_Click);
            // 
            // LogOutButton
            // 
            this.LogOutButton.Location = new System.Drawing.Point(64, 225);
            this.LogOutButton.Name = "LogOutButton";
            this.LogOutButton.Size = new System.Drawing.Size(61, 23);
            this.LogOutButton.TabIndex = 7;
            this.LogOutButton.Text = "Log Out";
            this.LogOutButton.UseVisualStyleBackColor = true;
            this.LogOutButton.Click += new System.EventHandler(this.LogOutButton_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 3600000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SocialCouponDisplabutton
            // 
            this.SocialCouponDisplabutton.Location = new System.Drawing.Point(228, 208);
            this.SocialCouponDisplabutton.Name = "SocialCouponDisplabutton";
            this.SocialCouponDisplabutton.Size = new System.Drawing.Size(183, 23);
            this.SocialCouponDisplabutton.TabIndex = 8;
            this.SocialCouponDisplabutton.Text = "Coupons from social network";
            this.SocialCouponDisplabutton.UseVisualStyleBackColor = true;
            this.SocialCouponDisplabutton.Click += new System.EventHandler(this.SocialCouponDisplabutton_Click);
            // 
            // CustumerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 260);
            this.Controls.Add(this.SocialCouponDisplabutton);
            this.Controls.Add(this.LogOutButton);
            this.Controls.Add(this.OrderedCouponsButton);
            this.Controls.Add(this.addFromSocialNetButton);
            this.Controls.Add(this.UpdateCategoriesButton);
            this.Controls.Add(this.ChangePasswordButton);
            this.Controls.Add(this.RateCouponsButton);
            this.Controls.Add(this.BusinessSearchButton);
            this.Controls.Add(this.SearchCouponsButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustumerWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grouponim";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CustumerWindow_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SearchCouponsButton;
        private System.Windows.Forms.Button BusinessSearchButton;
        private System.Windows.Forms.Button RateCouponsButton;
        private System.Windows.Forms.Button ChangePasswordButton;
        private System.Windows.Forms.Button UpdateCategoriesButton;
        private System.Windows.Forms.Button addFromSocialNetButton;
        private System.Windows.Forms.Button OrderedCouponsButton;
        private System.Windows.Forms.Button LogOutButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button SocialCouponDisplabutton;

    }
}