﻿namespace Windows
{
    partial class AddCouponFromSocialWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LinkTextBox = new System.Windows.Forms.TextBox();
            this.DescriptionText = new System.Windows.Forms.TextBox();
            this.Link = new System.Windows.Forms.Label();
            this.description = new System.Windows.Forms.Label();
            this.addSocialCoupon = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LinkTextBox
            // 
            this.LinkTextBox.Location = new System.Drawing.Point(62, 160);
            this.LinkTextBox.Name = "LinkTextBox";
            this.LinkTextBox.Size = new System.Drawing.Size(307, 20);
            this.LinkTextBox.TabIndex = 9;
            // 
            // DescriptionText
            // 
            this.DescriptionText.Location = new System.Drawing.Point(62, 75);
            this.DescriptionText.Name = "DescriptionText";
            this.DescriptionText.Size = new System.Drawing.Size(307, 20);
            this.DescriptionText.TabIndex = 8;
            // 
            // Link
            // 
            this.Link.AutoSize = true;
            this.Link.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Link.Location = new System.Drawing.Point(133, 108);
            this.Link.Name = "Link";
            this.Link.Size = new System.Drawing.Size(163, 25);
            this.Link.TabIndex = 7;
            this.Link.Text = "Link to Coupon:";
            // 
            // description
            // 
            this.description.AutoSize = true;
            this.description.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.description.Location = new System.Drawing.Point(150, 33);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(126, 25);
            this.description.TabIndex = 6;
            this.description.Text = "Description:";
            // 
            // addSocialCoupon
            // 
            this.addSocialCoupon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.addSocialCoupon.Location = new System.Drawing.Point(138, 205);
            this.addSocialCoupon.Name = "addSocialCoupon";
            this.addSocialCoupon.Size = new System.Drawing.Size(144, 43);
            this.addSocialCoupon.TabIndex = 5;
            this.addSocialCoupon.Text = "Add Coupon!";
            this.addSocialCoupon.UseVisualStyleBackColor = true;
            // 
            // AddCouponFromSocialWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 260);
            this.Controls.Add(this.LinkTextBox);
            this.Controls.Add(this.DescriptionText);
            this.Controls.Add(this.Link);
            this.Controls.Add(this.description);
            this.Controls.Add(this.addSocialCoupon);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddCouponFromSocialWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddCouponFromSocialWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox LinkTextBox;
        private System.Windows.Forms.TextBox DescriptionText;
        private System.Windows.Forms.Label Link;
        private System.Windows.Forms.Label description;
        private System.Windows.Forms.Button addSocialCoupon;
    }
}