﻿namespace Windows
{
    partial class AddBusinessWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BusinessIDTextBox = new System.Windows.Forms.TextBox();
            this.AddBusinessButton = new System.Windows.Forms.Button();
            this.BusinessAdressTextBox = new System.Windows.Forms.TextBox();
            this.BusinessNameTextBox = new System.Windows.Forms.TextBox();
            this.BusinessDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BusinessCityComboBox = new System.Windows.Forms.ComboBox();
            this.BusinessCategoryComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // BusinessIDTextBox
            // 
            this.BusinessIDTextBox.Location = new System.Drawing.Point(27, 69);
            this.BusinessIDTextBox.Name = "BusinessIDTextBox";
            this.BusinessIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.BusinessIDTextBox.TabIndex = 0;
            // 
            // AddBusinessButton
            // 
            this.AddBusinessButton.Location = new System.Drawing.Point(158, 188);
            this.AddBusinessButton.Name = "AddBusinessButton";
            this.AddBusinessButton.Size = new System.Drawing.Size(102, 23);
            this.AddBusinessButton.TabIndex = 1;
            this.AddBusinessButton.Text = "Add Business!";
            this.AddBusinessButton.UseVisualStyleBackColor = true;
            this.AddBusinessButton.Click += new System.EventHandler(this.AddBusinessButton_Click);
            // 
            // BusinessAdressTextBox
            // 
            this.BusinessAdressTextBox.Location = new System.Drawing.Point(158, 69);
            this.BusinessAdressTextBox.Name = "BusinessAdressTextBox";
            this.BusinessAdressTextBox.Size = new System.Drawing.Size(100, 20);
            this.BusinessAdressTextBox.TabIndex = 2;
            // 
            // BusinessNameTextBox
            // 
            this.BusinessNameTextBox.Location = new System.Drawing.Point(27, 131);
            this.BusinessNameTextBox.Name = "BusinessNameTextBox";
            this.BusinessNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.BusinessNameTextBox.TabIndex = 3;
            // 
            // BusinessDescriptionTextBox
            // 
            this.BusinessDescriptionTextBox.Location = new System.Drawing.Point(158, 131);
            this.BusinessDescriptionTextBox.Name = "BusinessDescriptionTextBox";
            this.BusinessDescriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.BusinessDescriptionTextBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Business ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(155, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(289, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "City";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(155, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Description";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(289, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Category";
            // 
            // BusinessCityComboBox
            // 
            this.BusinessCityComboBox.FormattingEnabled = true;
            this.BusinessCityComboBox.Items.AddRange(new object[] {
            "Ashdod",
            "AshKelon",
            "Beer Sheva",
            "Eilat",
            "Hadera",
            "Haifa",
            "Jerusalem",
            "Netanya",
            "Petah Tikva",
            "Rishon LeTzion",
            "Tel Aviv"});
            this.BusinessCityComboBox.Location = new System.Drawing.Point(277, 68);
            this.BusinessCityComboBox.Name = "BusinessCityComboBox";
            this.BusinessCityComboBox.Size = new System.Drawing.Size(109, 21);
            this.BusinessCityComboBox.TabIndex = 13;
            // 
            // BusinessCategoryComboBox
            // 
            this.BusinessCategoryComboBox.FormattingEnabled = true;
            this.BusinessCategoryComboBox.Items.AddRange(new object[] {
            "Babies, children and toys",
            "Car and Home Improvement",
            "Electronics and Computers",
            "Entertainment",
            "Food and beverage",
            "Health & Beauty",
            "House and Garden",
            "Household Cleaning",
            "Men",
            "Recreation and tourism",
            "Performances",
            "Sports and outdoor",
            "Watches and Jewelry",
            "Women"});
            this.BusinessCategoryComboBox.Location = new System.Drawing.Point(277, 131);
            this.BusinessCategoryComboBox.Name = "BusinessCategoryComboBox";
            this.BusinessCategoryComboBox.Size = new System.Drawing.Size(109, 21);
            this.BusinessCategoryComboBox.TabIndex = 14;
            // 
            // AddBusinessWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 260);
            this.Controls.Add(this.BusinessCategoryComboBox);
            this.Controls.Add(this.BusinessCityComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BusinessDescriptionTextBox);
            this.Controls.Add(this.BusinessNameTextBox);
            this.Controls.Add(this.BusinessAdressTextBox);
            this.Controls.Add(this.AddBusinessButton);
            this.Controls.Add(this.BusinessIDTextBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddBusinessWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Business";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox BusinessIDTextBox;
        private System.Windows.Forms.Button AddBusinessButton;
        private System.Windows.Forms.TextBox BusinessAdressTextBox;
        private System.Windows.Forms.TextBox BusinessNameTextBox;
        private System.Windows.Forms.TextBox BusinessDescriptionTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox BusinessCityComboBox;
        private System.Windows.Forms.ComboBox BusinessCategoryComboBox;
    }
}