﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;
using System.Timers;
using UI;


namespace Windows
{
    public partial class CustumerWindow : Form
    {

        CustomerClass customer;
        string City;
        Windows.Messages.MessageHandler MH;

        public CustumerWindow(CustomerClass customer, string City, Windows.Messages.MessageHandler mh)
        {
            this.City = City;
            this.customer = customer;
            this.MH = mh;
            InitializeComponent(); 
        }
        public void OpenNotification()
        {
            NotificationWindow notification = new NotificationWindow(customer, City);
            notification.Show();
        }

        public void checkForNotifications()
        {
            /*
             DataClasses1DataContext db = new DataClasses1DataContext();
             var queryPrefrence = from userPrefrence in db.UserPrefrences
                                  join coupon in db.Coupons on userPrefrence.Prefrence equals coupon.Category
                                  join business in db.Businesses on coupon.Business_ID equals business.ID
                                  where userPrefrence.Email == customer.getEmail() && coupon.Status == "active" && business.City == City
                                  select coupon;

             List<Coupon> coupons = queryPrefrence.ToList();
             if (coupons.Count > 0)
             {
                 int size = coupons.Count;
                 //choose random coupon to present
                 Random r = new Random();
                 int rInt = r.Next(1, size);
                 Coupon choosenCoupon = coupons.ElementAt(rInt-1);
                 CouponClass couponC = new CouponClass(choosenCoupon.ID, choosenCoupon.Name, choosenCoupon.Description,
                                                       choosenCoupon.Original_Price, choosenCoupon.Discount_Price,
                                                       choosenCoupon.Expired_Date, choosenCoupon.Status);
                 PrefrenceNotificationClass pNotification = new PrefrenceNotificationClass(couponC);
                 customer.setNotification(pNotification);
                 OpenNotification();
             }*/
        }

        private void SearchCouponsButton_Click(object sender, EventArgs e)
        {
            CouponSearchWindow couponSearchWindow = new CouponSearchWindow(customer,MH);
            couponSearchWindow.Show();
        }

        private void BusinessSearchButton_Click(object sender, EventArgs e)
        {
            BusinessSearchWindow businessSearchWindow = new BusinessSearchWindow(customer,MH);
            businessSearchWindow.Show();
        }

        private void OrderedCouponsButton_Click(object sender, EventArgs e)
        {
            CustumerOrderedCouponsWindow costumerOrderedCouponsWindow = new CustumerOrderedCouponsWindow(customer,MH);
            costumerOrderedCouponsWindow.Show();
        }

        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            ChangePasswordWindow changePasswordWindow = new ChangePasswordWindow(customer,MH);
            changePasswordWindow.Show();
        }

        private void UpdateCategoriesButton_Click(object sender, EventArgs e)
        {
            UpdateCategoriesWindow updateCategoriesWindow = new UpdateCategoriesWindow(customer,MH);
            updateCategoriesWindow.Show();
        }

        private void RateCouponsButton_Click(object sender, EventArgs e)
        {
            RateCouponsWindow rateCouponWindow = new RateCouponsWindow(customer, MH);
            rateCouponWindow.Show();
        }

        private void LogOutButton_Click(object sender, EventArgs e)
        {
            MainWindow main = new MainWindow(MH);
            main.Show();
            this.Close();
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            NotificationWindow notificationWindow = new NotificationWindow(customer, City);
            notificationWindow.Show();
        }

        private void CustumerWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainWindow main = new MainWindow(MH);
            main.Show();
        }

        private void addFromSocialNetButton_Click(object sender, EventArgs e)
        {
            AddCouponFromSocialWindow addcoupon = new AddCouponFromSocialWindow(MH);
            addcoupon.Show();
        }

        private void SocialCouponDisplabutton_Click(object sender, EventArgs e)
        {
            DisplaySocialCouponWindow socialCoupon = new DisplaySocialCouponWindow(MH);
        }
    }
}
