﻿namespace Windows
{
    partial class ShowCouponRatingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.oneStarLabel = new System.Windows.Forms.Label();
            this.Star2Label = new System.Windows.Forms.Label();
            this.Star3Label = new System.Windows.Forms.Label();
            this.Star4Label = new System.Windows.Forms.Label();
            this.Star5Label = new System.Windows.Forms.Label();
            this.Star1Label = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TotalRatingLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(34, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "1 ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(34, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(34, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(34, 170);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 24);
            this.label4.TabIndex = 3;
            this.label4.Text = "4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(34, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 24);
            this.label5.TabIndex = 4;
            this.label5.Text = "5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(22, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "STARS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(146, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "VOTES";
            // 
            // oneStarLabel
            // 
            this.oneStarLabel.AutoSize = true;
            this.oneStarLabel.Location = new System.Drawing.Point(171, 61);
            this.oneStarLabel.Name = "oneStarLabel";
            this.oneStarLabel.Size = new System.Drawing.Size(0, 13);
            this.oneStarLabel.TabIndex = 7;
            // 
            // Star2Label
            // 
            this.Star2Label.AutoSize = true;
            this.Star2Label.Location = new System.Drawing.Point(171, 101);
            this.Star2Label.Name = "Star2Label";
            this.Star2Label.Size = new System.Drawing.Size(13, 13);
            this.Star2Label.TabIndex = 8;
            this.Star2Label.Text = "0";
            // 
            // Star3Label
            // 
            this.Star3Label.AutoSize = true;
            this.Star3Label.Location = new System.Drawing.Point(171, 138);
            this.Star3Label.Name = "Star3Label";
            this.Star3Label.Size = new System.Drawing.Size(13, 13);
            this.Star3Label.TabIndex = 9;
            this.Star3Label.Text = "0";
            // 
            // Star4Label
            // 
            this.Star4Label.AutoSize = true;
            this.Star4Label.Location = new System.Drawing.Point(171, 178);
            this.Star4Label.Name = "Star4Label";
            this.Star4Label.Size = new System.Drawing.Size(13, 13);
            this.Star4Label.TabIndex = 10;
            this.Star4Label.Text = "0";
            // 
            // Star5Label
            // 
            this.Star5Label.AutoSize = true;
            this.Star5Label.Location = new System.Drawing.Point(171, 215);
            this.Star5Label.Name = "Star5Label";
            this.Star5Label.Size = new System.Drawing.Size(13, 13);
            this.Star5Label.TabIndex = 11;
            this.Star5Label.Text = "0";
            // 
            // Star1Label
            // 
            this.Star1Label.AutoSize = true;
            this.Star1Label.Location = new System.Drawing.Point(171, 61);
            this.Star1Label.Name = "Star1Label";
            this.Star1Label.Size = new System.Drawing.Size(13, 13);
            this.Star1Label.TabIndex = 12;
            this.Star1Label.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(35, 260);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 16);
            this.label8.TabIndex = 13;
            this.label8.Text = "Total Rating : ";
            // 
            // TotalRatingLabel
            // 
            this.TotalRatingLabel.AutoSize = true;
            this.TotalRatingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.TotalRatingLabel.Location = new System.Drawing.Point(155, 260);
            this.TotalRatingLabel.Name = "TotalRatingLabel";
            this.TotalRatingLabel.Size = new System.Drawing.Size(16, 18);
            this.TotalRatingLabel.TabIndex = 14;
            this.TotalRatingLabel.Text = "0";
            // 
            // ShowCouponRatingWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 294);
            this.Controls.Add(this.TotalRatingLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Star1Label);
            this.Controls.Add(this.Star5Label);
            this.Controls.Add(this.Star4Label);
            this.Controls.Add(this.Star3Label);
            this.Controls.Add(this.Star2Label);
            this.Controls.Add(this.oneStarLabel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ShowCouponRatingWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rating";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label oneStarLabel;
        private System.Windows.Forms.Label Star2Label;
        private System.Windows.Forms.Label Star3Label;
        private System.Windows.Forms.Label Star4Label;
        private System.Windows.Forms.Label Star5Label;
        private System.Windows.Forms.Label Star1Label;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label TotalRatingLabel;
    }
}