﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;
using System.Web;

namespace Windows
{
    public partial class MainWindow : Form
    {
        Windows.Messages.MessageHandler MH;
        UserClass LoggedInUser;
        
        public MainWindow(Windows.Messages.MessageHandler mh)
        {
            InitializeComponent();
            this.MH = mh;
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            RegisterWindow Reg = new RegisterWindow(MH);
            Reg.Show();
        }

        private void LogInButton_Click(object sender, EventArgs e)
        {
            string City = CityBox.Text;
            string password = PasswordTextBox.Text;
            string email = EmailTextBox.Text;
            
            char type = MH.LogIn(City, password, email);

                if (type == 'a'){
                    LoggedInUser = MH.GetAdmin(email);
                    AdminWindow adminWindow = new AdminWindow((AdminClass)LoggedInUser,MH);
                    this.Hide();
                    adminWindow.Show();                    
                }
                else if (type == 'b'){
                    LoggedInUser = MH.GetBusinessOwner(email);
                    BusinessOwnerWindow businessOwnerWindow = new BusinessOwnerWindow((BusinessOwnerClass)LoggedInUser,MH);
                    this.Hide();
                    businessOwnerWindow.Show();
                }
                else if (type == 'c')
                {
                    LoggedInUser = MH.GetCustumer(email);
                    CustumerWindow customerWindow = new CustumerWindow((CustomerClass)LoggedInUser,City,MH);
                    this.Hide();
                    customerWindow.Show();
                }
                else
                {
                    MessageBox.Show("Incorrect Details", "Wrong Input");
                }
        }
      

        private void ForgetPasswordButton_Click(object sender, EventArgs e)
        {
            PasswordRecoveryWindow passwordRecoveryForm = new PasswordRecoveryWindow(MH);
            passwordRecoveryForm.Activate();
            passwordRecoveryForm.Show();
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
/*
        private void timer1_Tick(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            DateTime CurrentDate = DateTime.Now;
            DateTime LastYearDate = new DateTime(CurrentDate.Year - 1, CurrentDate.Month, CurrentDate.Day);

            var queryDelete = from Coupon in db.Coupons
                             where Coupon.Expired_Date.Date <= LastYearDate.Date
                             select Coupon;

            foreach (var Del in queryDelete)
            {
                db.Coupons.DeleteOnSubmit(Del);
            }

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // Provide for exceptions.
            }
        }

        */
       
   }
}
