﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Windows
{
    public partial class AddBusinessWindow : Form
    {
        Windows.Messages.MessageHandler MH;

        public AddBusinessWindow(Windows.Messages.MessageHandler MH)
        {
            this.MH = MH;
            InitializeComponent();
        }

        private void AddBusinessButton_Click(object sender, EventArgs e)
        {
            string address = BusinessAdressTextBox.Text;
            string description = BusinessDescriptionTextBox.Text;
            string ID = BusinessIDTextBox.Text;
            string Name = BusinessNameTextBox.Text;
            string category = BusinessCategoryComboBox.Text;
            string city = BusinessCityComboBox.Text;

            if (MH.AddNewBusiness(address, description, ID, Name, category, city))
            {
                MessageBox.Show("Business added!");
                this.Close();
            }
            else
            {
                MessageBox.Show("faild to add new business! check your inputs and try again!");
            }
            /*

            if (BusinessAdressTextBox.Text != "" && BusinessDescriptionTextBox.Text != "" && BusinessIDTextBox.Text != "" &&
                BusinessNameTextBox.Text != "" && BusinessCategoryComboBox.Text != "" && BusinessCityComboBox.Text != "")
            {
                Business business = new Business
                {
                    ID = Convert.ToInt32(BusinessIDTextBox.Text),
                    Address = BusinessAdressTextBox.Text,
                    City = BusinessCityComboBox.Text,
                    Name = BusinessNameTextBox.Text,
                    Description = BusinessDescriptionTextBox.Text,
                    Category = BusinessCategoryComboBox.Text
                };

                db.Businesses.InsertOnSubmit(business);
                db.SubmitChanges();
                MessageBox.Show("Business added");
                this.Close();

            }
            else
            {
                MessageBox.Show("one of your inputs is wrong", "");
            }
             * */
        }
    }
}
