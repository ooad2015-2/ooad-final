﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;

namespace Windows
{
    public partial class OwnersCouponsRatingWindow : Form
    {
        BusinessOwnerClass owner;
        Windows.Messages.MessageHandler MH;

        public OwnersCouponsRatingWindow(BusinessOwnerClass owner, Windows.Messages.MessageHandler mh)
        {
            this.owner = owner;
            this.MH = mh;
            InitializeComponent();
            initializeGrid();
        }

        public void initializeGrid()
        {
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = MH.DisplayRatedCoupons(owner.getEmail());
            dataGridView1.DataSource = bindingSource1;
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ShowCouponRatingWindow ratingWindow = new ShowCouponRatingWindow(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString()), MH);
            ratingWindow.Show();
        }
    }
}
