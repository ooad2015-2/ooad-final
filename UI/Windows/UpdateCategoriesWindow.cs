﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BackEnd;
namespace Windows
{
    public partial class UpdateCategoriesWindow : Form
    {
        CustomerClass customer;
        Windows.Messages.MessageHandler MH;

        public UpdateCategoriesWindow(CustomerClass customer, Windows.Messages.MessageHandler mh)
        {
            this.customer = customer;
            this.MH = mh;
            InitializeComponent();
            initializeCheckBox();
        }

        public void initializeCheckBox()
        {
          for (int i = 0; i < checkedListBox1.Items.Count; i++)
           {
                string category = checkedListBox1.Items[i].ToString();               
                if (MH.CheckIfUserHaveCategory(category, customer.getEmail()))
                {
                    checkedListBox1.SetItemChecked(i, true);
                }               
           }
        }

        private void UpdateCategoriesButton_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                bool ch = checkedListBox1.GetItemChecked(i);
                MH.UpdatePreference(customer.getEmail(), checkedListBox1.Items[i].ToString(), ch);  
            }

            this.Close();
        }
    }
}
