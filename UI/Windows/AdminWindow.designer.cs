﻿namespace Windows
{
    partial class AdminWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CouponsApprovalButton = new System.Windows.Forms.Button();
            this.CouponListButton = new System.Windows.Forms.Button();
            this.BusinessesListButton = new System.Windows.Forms.Button();
            this.AddNewBusinessButton = new System.Windows.Forms.Button();
            this.AddNewBusinessOwnerButton = new System.Windows.Forms.Button();
            this.ChangePasswordButton = new System.Windows.Forms.Button();
            this.LogOutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CouponsApprovalButton
            // 
            this.CouponsApprovalButton.Location = new System.Drawing.Point(44, 66);
            this.CouponsApprovalButton.Name = "CouponsApprovalButton";
            this.CouponsApprovalButton.Size = new System.Drawing.Size(144, 23);
            this.CouponsApprovalButton.TabIndex = 0;
            this.CouponsApprovalButton.Text = "Coupons  for Approval";
            this.CouponsApprovalButton.UseVisualStyleBackColor = true;
            this.CouponsApprovalButton.Click += new System.EventHandler(this.CouponsApprovalButton_Click);
            // 
            // CouponListButton
            // 
            this.CouponListButton.Location = new System.Drawing.Point(44, 117);
            this.CouponListButton.Name = "CouponListButton";
            this.CouponListButton.Size = new System.Drawing.Size(144, 23);
            this.CouponListButton.TabIndex = 1;
            this.CouponListButton.Text = "Coupons List";
            this.CouponListButton.UseVisualStyleBackColor = true;
            this.CouponListButton.Click += new System.EventHandler(this.CouponListButton_Click);
            // 
            // BusinessesListButton
            // 
            this.BusinessesListButton.Location = new System.Drawing.Point(44, 170);
            this.BusinessesListButton.Name = "BusinessesListButton";
            this.BusinessesListButton.Size = new System.Drawing.Size(144, 23);
            this.BusinessesListButton.TabIndex = 2;
            this.BusinessesListButton.Text = "Business List";
            this.BusinessesListButton.UseVisualStyleBackColor = true;
            this.BusinessesListButton.Click += new System.EventHandler(this.BusinessesListButton_Click);
            // 
            // AddNewBusinessButton
            // 
            this.AddNewBusinessButton.Location = new System.Drawing.Point(238, 66);
            this.AddNewBusinessButton.Name = "AddNewBusinessButton";
            this.AddNewBusinessButton.Size = new System.Drawing.Size(144, 23);
            this.AddNewBusinessButton.TabIndex = 3;
            this.AddNewBusinessButton.Text = "Add New Business";
            this.AddNewBusinessButton.UseVisualStyleBackColor = true;
            this.AddNewBusinessButton.Click += new System.EventHandler(this.AddNewBusinessButton_Click);
            // 
            // AddNewBusinessOwnerButton
            // 
            this.AddNewBusinessOwnerButton.Location = new System.Drawing.Point(238, 117);
            this.AddNewBusinessOwnerButton.Name = "AddNewBusinessOwnerButton";
            this.AddNewBusinessOwnerButton.Size = new System.Drawing.Size(144, 23);
            this.AddNewBusinessOwnerButton.TabIndex = 4;
            this.AddNewBusinessOwnerButton.Text = "Add New Business Owner";
            this.AddNewBusinessOwnerButton.UseVisualStyleBackColor = true;
            this.AddNewBusinessOwnerButton.Click += new System.EventHandler(this.AddNewBusinessOwnerButton_Click);
            // 
            // ChangePasswordButton
            // 
            this.ChangePasswordButton.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.ChangePasswordButton.Location = new System.Drawing.Point(238, 170);
            this.ChangePasswordButton.Name = "ChangePasswordButton";
            this.ChangePasswordButton.Size = new System.Drawing.Size(144, 23);
            this.ChangePasswordButton.TabIndex = 5;
            this.ChangePasswordButton.Text = "Change Password";
            this.ChangePasswordButton.UseVisualStyleBackColor = true;
            this.ChangePasswordButton.UseWaitCursor = true;
            this.ChangePasswordButton.Click += new System.EventHandler(this.ChangePasswordButton_Click);
            // 
            // LogOutButton
            // 
            this.LogOutButton.Location = new System.Drawing.Point(44, 225);
            this.LogOutButton.Name = "LogOutButton";
            this.LogOutButton.Size = new System.Drawing.Size(61, 23);
            this.LogOutButton.TabIndex = 8;
            this.LogOutButton.Text = "Log Out";
            this.LogOutButton.UseVisualStyleBackColor = true;
            this.LogOutButton.Click += new System.EventHandler(this.LogOutButton_Click);
            // 
            // AdminWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 260);
            this.Controls.Add(this.LogOutButton);
            this.Controls.Add(this.ChangePasswordButton);
            this.Controls.Add(this.AddNewBusinessOwnerButton);
            this.Controls.Add(this.AddNewBusinessButton);
            this.Controls.Add(this.BusinessesListButton);
            this.Controls.Add(this.CouponListButton);
            this.Controls.Add(this.CouponsApprovalButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdminWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grouponim";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminWindow_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CouponsApprovalButton;
        private System.Windows.Forms.Button CouponListButton;
        private System.Windows.Forms.Button BusinessesListButton;
        private System.Windows.Forms.Button AddNewBusinessButton;
        private System.Windows.Forms.Button AddNewBusinessOwnerButton;
        private System.Windows.Forms.Button ChangePasswordButton;
        private System.Windows.Forms.Button LogOutButton;
    }
}