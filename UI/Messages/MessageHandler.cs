﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Service;
using BackEnd;

namespace Windows.Messages
{
    public class MessageHandler
    {
        ServiceApp App;

        public MessageHandler()
        {
            this.App = new ServiceApp();
        }

        public char LogIn(string city, string password, string email)
        {
            //returns the user type if exist else returns char 'f' for fail
            return App.LogIn(city, password, email);
        }

        public AdminClass GetAdmin(string email)
        {
            return App.GetAdmin(email);
        }

        public BusinessOwnerClass GetBusinessOwner(string email)
        {
            return App.GetBusinessOwner(email);
        }

        public CustomerClass GetCustumer(string email)
        {
            return App.GetCustomer(email);
        }

        public Boolean CheckIfUserExist(string email)
        {
            //return true if user exist  and false if not exist
            return App.CheckIfUserExist(email);
        }

        public Boolean RegisterCustumer(string email, string password, string userName, int phone, List<String> userPrefrence)
        {
            return App.RegisterCustumer(email, password, userName, phone, userPrefrence);
        }

        public string ForgetPassword(string email)
        {
            return App.ForgetPassword(email);
        }

        public string UpdatePassword(string email, string oldPassword, string newPassword)
        {
            return App.UpdatePassword(email, oldPassword, newPassword);
;
        }



        public List<DisplayedCoupon> DisplayCoupons(string category, string city)
        {
            return App.DisplayCoupons(category, city);
        }

        public List<DisplayedBusiness> DisplayBusinesses(string category, string city)
        {
            return App.DisplayBusinesses(category, city);
        }

        public List<DisplayedCoupon> CouponsToApprove()
        {
            return App.CouponsToApprove();
        }

        public Boolean ApproveCoupon(int couponID)
        {
            return App.ApproveCoupon(couponID);
        }

        public Boolean DeleteCoupon(int couponID)
        {
            return App.DeleteCoupon(couponID);
        }

        public DisplayedBusiness GetBusiness(int businessID)
        {
            return App.GetBusiness(businessID);
        }

        public Boolean DeleteBusiness(int BusinessID)
        {
            return App.DeleteBusiness(BusinessID);
        }

        public Boolean AddNewBusiness(string address, string description, string ID, string Name, string category, string city)
        {
            return App.AddNewBusiness(address, description, ID, Name, category, city);
        }

        public Boolean AddBusinessOwner(int Id, string email, string userName, int phone, string password)
        {
            return App.AddBusinessOwner(Id, email, userName, phone, password);
        }

        public DisplayedCoupon GetCoupon(int couponId)
        {
            return App.GetCoupon(couponId);
        }

        public Boolean OrderCoupon(int couponId, CustomerClass customer)
        {
            return App.OrderCoupon(couponId, customer);
        }


        public  DisplayedCoupon DisplayCoupons(int CouponID)
        {
            return App.DisplayCoupons(CouponID);
        }

        public Boolean ChangeCoupon(int CouponID, string newName, string newCategory, string newDescription, int newDiscountPrice, DateTime newExpiredDate)
        {
            return App.ChangeCoupon(CouponID, newName, newCategory, newDescription, newDiscountPrice, newExpiredDate);
        }

        public List<DisplayOrder> DisplayOrders(string email)
        {
            return App.DisplayOrders(email);
        }

        public Boolean CheckIfUserHaveCategory(string category, string email)
        {
            return App.CheckIfUserHaveCategory(category,email);
        }

        public void UpdatePreference(string email, string preference, bool checkedP)
        {
             App.UpdatePreference(email, preference, checkedP);
        }

        public List<DisplayRatedCoupons> ToRateCoupons(string email)
        {
            return App.ToRateCoupons(email);
        }

        public bool RateCoupon(string email, int couponID, int rating)
        {
            return App.RateCoupon(email, couponID, rating);
        }

        public List<DisplayOrderedCoupons> DisplayCouponOrders(string email)
        {
            return App.DisplayCouponOrders(email);
        }

        public bool AddCoupon(int businessId, int originalPrice, int discountPrice, string name, string description, string category, DateTime expiredDate)
        {
            return App.AddCoupon(businessId, originalPrice, discountPrice, name, description, category, expiredDate);
        }

        public bool UpdateCoustomerUsedCoupon(int serialKey, string email)
        {
            return App.UpdateCoustomerUsedCoupon(serialKey, email);
        }

        public List<DisplayRatedCoupon> DisplayRatedCoupons(string email)
        {
            return App.DisplayRatedCoupons(email);
        }

        public List<int> getRating(int couponID)
        {
            return App.getRating(couponID);
        }

        public bool AddSocialCoupon(string description, string link)
        {
            return App.AddSocialCoupon(description, link);
        }

        public List<DisplaySocialCoupon> DisplaySocialCoupons()
        {
            return App.DisplaySocialCoupons();
        }
    }
}
