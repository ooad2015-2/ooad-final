﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class CustomerClass : UserClass
    {
        private List<OrderClass> Orders { get; set; }
        private NotificationClass Notification { get; set; }

        public CustomerClass(int Phone, String UserName, String Email ,List<OrderClass> Orders)
        {
            this.Email = Email;
            this.Phone = Phone;
            this.UserName = UserName;
            this.Orders = Orders;
        }
        public List<OrderClass>  getOrdersList()
        {
            return Orders;
        }

        public NotificationClass getNotification()
        {
            return Notification;
        }

        public void setNotification(NotificationClass notification)
        {
            this.Notification = notification;
        }

        public void AddNewOrder(OrderClass order)
        {
            Orders.Add(order);
        }
    }
}
