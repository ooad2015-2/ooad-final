﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class CouponClass
    {
        private int ID { get; set; }
        private string Name { get; set; }
        private string Status { get; set; }
        private String Description { get; set; }
        private Double OriginalPrice { get; set; }
        private Double OfferPrice { get; set; }
        private DateTime ExpireDate { get; set; }



        public CouponClass(int ID, String Name, String Description, Double OriginalPrice, Double OfferPrice, DateTime ExpireDate, string Status)
        {
            this.ID = ID;
            this.Status = Status;
            this.Name = Name;
            this.Description = Description;
            this.OriginalPrice = OriginalPrice;
            this.OfferPrice = OfferPrice;
            this.ExpireDate = ExpireDate;
        }

        public int getID()
        {
            return ID;
        }

        public string getName()
        {
            return Name;
        }

        public string getStatus()
        {
            return Status;
        }

        public string getDescription()
        {
            return Description;
        }

        public double getOriginalPrice()
        {
            return OriginalPrice;
        }

        public double getOfferPrice()
        {
            return OfferPrice;
        }

        public DateTime getExpireDate()
        {
            return ExpireDate;
        }


    }
}
