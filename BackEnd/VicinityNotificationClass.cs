﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class VicinityNotificationClass : NotificationClass
    {
        private CouponClass NeerByCoupon { get; set; }

        public CouponClass getCouponNotification()
        {
            return NeerByCoupon;
        }
    }
}
