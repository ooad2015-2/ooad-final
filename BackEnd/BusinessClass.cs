﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class BusinessClass
    {
        private int ID { get; set; }
        private string Name { get; set; }
        private String Address { get; set; }
        private String City { get; set; }
        private String Description { get; set; }
        private String Category { get; set; }
       // private List<CouponClass> CouponsList { get; set; }


        public BusinessClass(int ID, string Name, String Address, String City,string Description, string Category) //,List<CouponClass> CouponsList)
        {
            this.Address = Address;
            this.ID = ID;
            this.Name = Name;
            this.City = City;
            this.Description = Description;
            this.Category = Category;
           // this.CouponsList = CouponsList;
        }
    }
}
