﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class PrefrenceNotificationClass : NotificationClass
    {
        private CouponClass PrefrenceCoupon { get; set; }

        public PrefrenceNotificationClass(CouponClass prefrenceCoupon)
        {
            this.PrefrenceCoupon = prefrenceCoupon;
        }

        public CouponClass getCouponNotification()
        {
            return PrefrenceCoupon;
        }
    }
}
