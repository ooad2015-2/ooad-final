﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class DisplaySocialCoupon
    {
        public string link  {get; set; }
        public string description { get; set; }
    }
}
