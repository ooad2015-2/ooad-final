﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public abstract class UserClass
    {
        
        protected int Phone { get; set; }
        protected string UserName { get; set; }
        protected string Email { get; set; }  
        protected char Type { get; set; }

        //constructor
        public UserClass(int phone, string userName, string email, char type)
        {
            Phone = phone;
            UserName = userName;
            Email = email;
            Type = type;
        }

        public UserClass()
        {
        }

        public int getPhone()
        {
            return Phone;
        }

        public string getUserName()
        {
            return UserName;
        }

        public string getEmail()
        {
            return Email;
        }

        public char getUserType()
        {
            return Type;
        }


    }


}
