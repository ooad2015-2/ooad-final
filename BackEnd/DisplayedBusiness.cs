﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class DisplayedBusiness
    {
        public int Business_ID  {get; set; }
        public string Business_Name {get; set; }
        public string Category {get; set; }
        public string Description {get; set; }
        public string Address {get; set; }
        public string City { get; set; }
    }
}
