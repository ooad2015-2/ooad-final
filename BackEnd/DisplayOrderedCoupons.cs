﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class DisplayOrderedCoupons
    {
        public int Business_ID { get; set; }
        public int Coupon_ID { get; set; }
        public string Coupon_Name { get; set; }
        public string User_Email { get; set; }
        public int Order_SerialKey { get; set; }
        public DateTime Order_Date { get; set; }
        public int Order_Price_Paid { get; set; }
        public string Order_Status { get; set; }
    }
}
