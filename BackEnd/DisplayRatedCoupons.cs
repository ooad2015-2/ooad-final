﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class DisplayRatedCoupons
    {
        public string Coupon_Name {get; set; }
        public int Coupon_ID { get; set; }
        public string Category { get; set; }
        public string Business_Name { get; set; }
        public string Description { get; set; }
        public DateTime Order_Date { get; set; }
    }
}
