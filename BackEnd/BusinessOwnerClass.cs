﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class BusinessOwnerClass : UserClass
    {
        private List<BusinessClass> BusinessID;

        public BusinessOwnerClass(List<BusinessClass> BusinessID, int Phone, String UserName, String Email)
        {
            this.BusinessID = BusinessID;
            this.Email = Email;
            this.Phone = Phone;
            this.UserName = UserName;
        }
    }
}
