﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class OrderClass
    {
            private int SerialKey{get; set;}
            private DateTime OrderDate {get; set;}
            private string Status { get; set; }
            private CouponClass OrderCoupon {get; set;}

            public OrderClass(int serialKey, DateTime orderDate, string status, CouponClass orderCoupon)
            {
                this.SerialKey = serialKey;
                this.OrderDate = orderDate;
                this.Status = status;
                this.OrderCoupon = OrderCoupon;
            }
    }
}
