﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class AdminClass : UserClass
    {

        public AdminClass(int Phone, String UserName, String Email)
        {
            this.Email = Email;
            this.Phone = Phone;
            this.UserName = UserName;
        }
    }
}
