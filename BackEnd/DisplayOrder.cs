﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd
{
    public class DisplayOrder
    {
        public int Serial {get; set; }
        public string Coupon_Name { get; set; }
        public string Category { get; set; }
        public string Business_Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime Order_Date { get; set; }
        public int Price_Paid { get; set; }
        public DateTime Expired_Date { get; set; }
    }
}
