﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackEnd;
using BL;

namespace Service
{
    public class ServiceApp
    {
        private Facade facade;

        public ServiceApp()
        {
            this.facade = new Facade();
        }

        public char LogIn(string city, string password, string email)
        {
            return facade.LogIn(city,password,email);
        }

        public AdminClass GetAdmin(string email)
        {
            return facade.GetAdmin(email);
        }

        public BusinessOwnerClass GetBusinessOwner(string email)
        {
            return facade.GetBusinessOwner(email);
        }

        public CustomerClass GetCustomer(string email)
        {
            return facade.GetCustomer(email);
        }

        public Boolean CheckIfUserExist(string email)
        {
            return facade.CheckIfUserExist(email);
        }

        public Boolean RegisterCustumer(string email, string password, string userName, int phone, List<String> userPrefrence)
        {
            return facade.RegisterCustumer(email,password,userName,phone, userPrefrence); 
        }

        public string ForgetPassword(string email)
        {
            return facade.ForgetPassword(email);
        }

        public string UpdatePassword(string email, string oldPassword, string newPassword)
        {
            return facade.UpdatePassword(email, oldPassword, newPassword);
        }

        public List<DisplayedCoupon> DisplayCoupons(string category, string city)
        {
            return facade.DisplayCoupons(category, city);
        }

        public List<DisplayedBusiness> DisplayBusinesses(string category, string city)
        {
            return facade.DisplayBusinesses(category, city);
        }

        public List<DisplayedCoupon> CouponsToApprove()
        {
            return facade.CouponsToApprove();
        }

        public Boolean ApproveCoupon(int couponID)
        {
            return facade.ApproveCoupon(couponID);
        }

        public bool DeleteCoupon(int couponID)
        {
            return facade.DeleteCoupon(couponID);
        }

        public DisplayedBusiness GetBusiness(int businessID)
        {
            return facade.GetBusiness(businessID);
        }

        public Boolean DeleteBusiness(int BusinessID)
        {
            return facade.DeleteBusiness(BusinessID);
        }

        public Boolean AddNewBusiness(string address, string description, string ID, string Name, string category, string city)
        {
            return facade.AddNewBusiness(address, description, ID, Name, category, city);
;
        }

        public Boolean AddBusinessOwner(int Id, string email, string userName, int phone, string password)
        {
            return facade.AddBusinessOwner(Id, email, userName, phone, password);
        }

        public DisplayedCoupon GetCoupon(int couponId)
        {
            return facade.GetCoupon(couponId);
        }



        public Boolean OrderCoupon(int couponId, CustomerClass customer)
        {
            return facade.OrderCoupon(couponId, customer);
        }

        public DisplayedCoupon DisplayCoupons(int CouponID)
        {
            return facade.DisplayCoupons(CouponID);
        }

        public Boolean ChangeCoupon(int CouponID, string newName, string newCategory, string newDescription, int newDiscountPrice, DateTime newExpiredDate)
        {
            return facade.ChangeCoupon(CouponID, newName, newCategory, newDescription, newDiscountPrice, newExpiredDate);
        }

        public List<DisplayOrder> DisplayOrders(string email)
        {
            return facade.DisplayOrders(email);
        }

        public Boolean CheckIfUserHaveCategory(string category, string email)
        {
            return facade.CheckIfUserHaveCategory(category, email);
        }

        public void UpdatePreference(string email, string preference, bool checkedP)
        {
             facade.UpdatePreference(email, preference, checkedP);
        }

        public List<DisplayRatedCoupons> ToRateCoupons(string email)
        {
            return facade.ToRateCoupons(email);
        }


        public bool RateCoupon(string email, int couponID, int rating)
        {
            return facade.RateCoupon(email, couponID, rating);
        }

        public List<DisplayOrderedCoupons> DisplayCouponOrders(string email)
        {
            return facade.DisplayCouponOrders(email);
        }

        public bool AddCoupon(int businessId, int originalPrice, int discountPrice, string name, string description, string category, DateTime expiredDate)
        {
            return facade.AddCoupon(businessId, originalPrice, discountPrice, name, description, category, expiredDate);
        }

        public bool UpdateCoustomerUsedCoupon(int serialKey, string email)
        {
            return facade.UpdateCoustomerUsedCoupon(serialKey, email);
        }

        public List<DisplayRatedCoupon> DisplayRatedCoupons(string email)
        {
            return facade.DisplayRatedCoupons(email);
        }

        public List<int> getRating(int couponID)
        {
            return facade.getRating(couponID);
        }

        public bool AddSocialCoupon(string description, string link)
        {
            return facade.AddSocialCoupon(description, link);
        }

        public List<DisplaySocialCoupon> DisplaySocialCoupons()
        {
            return facade.DisplaySocialCoupons();
        }
    }
}
